<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('customer_id');
            $table->unsignedInteger('event');
            $table->jsonb('channels');
            $table->string('theme')->nullable(true);
            $table->text('text');
            $table->boolean('is_viewed');
            $table->timestamps(6);
        });

        Schema::create('notification_settings', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->unsignedInteger('event')->unique();
            $table->jsonb('channels');
            $table->string('theme')->nullable(true);
            $table->text('text');
            $table->timestamps(6);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('notifications');
        Schema::dropIfExists('notification_settings');
    }
};
