<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('themes', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->boolean('active')->default(true);
            $table->json('channel')->nullable();

            $table->timestamps();
        });

        Schema::create('types', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->boolean('active')->default(true);
            $table->json('channel')->nullable();

            $table->timestamps();
        });

        Schema::create('statuses', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->boolean('active')->default(true);
            $table->boolean('default')->default(false);
            $table->json('channel')->nullable();

            $table->timestamps();
        });

        Schema::create('channels', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');

            $table->timestamps();
        });

        Schema::create('attachments', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('path');
            $table->string('disk');
            $table->string('url');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('themes');
        Schema::dropIfExists('types');
        Schema::dropIfExists('statuses');
        Schema::dropIfExists('channels');
        Schema::dropIfExists('attachments');
    }
};
