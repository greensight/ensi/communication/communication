<?php

namespace App\Domain\Events\Mail;

class OrderCancelled extends AbstractOrderMail
{
    public function build()
    {
        return $this->view('mail/order_cancelled');
    }
}
