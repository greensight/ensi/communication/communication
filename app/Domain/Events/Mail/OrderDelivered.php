<?php

namespace App\Domain\Events\Mail;

class OrderDelivered extends AbstractOrderMail
{
    public function build()
    {
        return $this->view('mail/order_delivered');
    }
}
