<?php

namespace App\Domain\Events\Mail\User;

use Illuminate\Mail\Mailable;

class UserUpdated extends Mailable
{
    public string $fullName;

    public function __construct(array $data)
    {
        $this->fullName = $data['full_name'];
        $this->subject = $data['subject'];
    }

    public function build(): self
    {
        return $this->view('mail/user/user_updated');
    }
}
