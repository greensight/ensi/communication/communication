<?php

namespace App\Domain\Events\Mail;

class ManagerOrderPaid extends AbstractOrderMail
{
    public function build()
    {
        return $this->view('mail/manager_order_paid');
    }
}
