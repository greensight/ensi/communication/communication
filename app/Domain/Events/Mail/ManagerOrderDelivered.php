<?php

namespace App\Domain\Events\Mail;

class ManagerOrderDelivered extends AbstractOrderMail
{
    public function build()
    {
        return $this->view('mail/manager_order_delivered');
    }
}
