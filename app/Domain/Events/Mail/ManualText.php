<?php

namespace App\Domain\Events\Mail;

use Illuminate\Mail\Mailable;

class ManualText extends Mailable
{
    public $text;

    public function __construct($data)
    {
        $this->text = $data['text'];
        $this->subject = $data['subject'];
    }

    public function build()
    {
        return $this->view('mail/manual');
    }
}
