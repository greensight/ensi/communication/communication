<?php

namespace App\Domain\Events\Mail;

use Illuminate\Mail\Mailable;

class SendReferralDocument extends Mailable
{
    public $first_name;
    public $middle_name;
    public $document_id;
    public $document_period_since;
    public $document_period_to;
    public $document_creation_date;
    public $document_amount_reward;
    public $document_status;
    public $file_url;

    public $subject = 'Новый акт о реферальных зачислениях';

    public function __construct(array $data = [])
    {
        $this->first_name = $data['u_first_name'];
        $this->middle_name = $data['u_mid_name'];
        $this->document_id = $data['d_id'];
        $this->document_period_since = $data['d_period_since'];
        $this->document_period_to = $data['d_period_to'];
        $this->document_creation_date = $data['d_creation_date'];
        $this->document_amount_reward = $data['d_amount_reward'];
        $this->document_status = $data['d_status'];
        $this->file_url = $data['file_url'];
    }

    public function build()
    {
        return $this->view('mail/send_referral_document');
    }
}
