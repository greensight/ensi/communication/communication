<?php

namespace App\Domain\Events\Actions\Customers\Customers;

use App\Domain\Events\Mail\Customers\CustomersChangeEmail;
use Ensi\CustomerAuthClient\ObjectSerializer;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\MessageForChangeEmail;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Illuminate\Support\Facades\Mail;
use RdKafka\Message;
use Throwable;

class ChangeEmailAction
{
    public function __construct(private CustomersApi $customersApi)
    {
        //
    }

    /** @throws Throwable */
    public function execute(Message $kafkaMessage)
    {
        /** @var MessageForChangeEmail $message */
        $message = ObjectSerializer::deserialize($kafkaMessage->payload, MessageForChangeEmail::class);
        $request = new SearchCustomersRequest([
            'filter' => [
                'id' => $message->getCustomerId(),
            ],
        ]);
        $customer = $this->customersApi->searchCustomers($request)->getData()[0];

        Mail::to($customer->getEmail())->send(new CustomersChangeEmail([
            'full_name' => $customer->getFirstName() . ' ' . $customer->getLastName(),
            'link' => config('internal-services.front_urls.set_password') . $message->getToken(),
            'new_email' => $message->getNewEmail(),
            'subject' => __('messages.customers.change_email.subject'),
        ]));
    }
}
