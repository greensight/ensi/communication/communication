<?php

namespace App\Domain\Events\Actions\Customers\Customers\SendMail;

use App\Domain\Events\Mail\Customers\Customers\CustomerUpdated;
use Ensi\CustomersClient\Api\CustomersApi;
use Illuminate\Support\Facades\Mail;
use RdKafka\Message;

class CustomerUpdatedAction
{
    public function __construct(
        private CustomersApi $customersApi,
    ) {
    }

    public function execute(Message $message)
    {
        $message = json_decode($message->payload);

        $customer = $this->customersApi->getCustomer($message->customer_id)->getData();

        $attributes = [];
        foreach ($message->old_data as $oldAttributeKey => $oldAttributeValue) {
            $camelAttributeName = snake_to_camel_case($oldAttributeKey, true);
            $getterName = "get{$camelAttributeName}";
            $attributes[$oldAttributeKey] = [
                'old' => $oldAttributeValue,
                'new' => $customer->{$getterName}(),
            ];
        }

        Mail::to($customer->getEmail())->send(new CustomerUpdated([
            'subject' => __('messages.customers.customers.customer_updated.subject'),
            'full_name' => $customer->getFullName(),
            'attributes' => $attributes,
        ]));
    }
}
