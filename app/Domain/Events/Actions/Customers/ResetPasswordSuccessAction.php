<?php

namespace App\Domain\Events\Actions\Customers;

use App\Domain\Events\Mail\User\UserResetPasswordSuccess;
use Ensi\CustomerAuthClient\Dto\MessageForResetPasswordSuccess;
use Ensi\CustomerAuthClient\ObjectSerializer;
use Illuminate\Support\Facades\Mail;
use RdKafka\Message;
use Throwable;

class ResetPasswordSuccessAction
{
    /** @throws Throwable */
    public function execute(Message $kafkaMessage)
    {
        /** @var MessageForResetPasswordSuccess $message */
        $message = ObjectSerializer::deserialize($kafkaMessage->payload, MessageForResetPasswordSuccess::class);

        Mail::to($message->getUserEmail())->send(new UserResetPasswordSuccess([
            'full_name' => $message->getUserFullName(),
            'subject' => __('messages.users.reset_password_success.subject'),
        ]));
    }
}
