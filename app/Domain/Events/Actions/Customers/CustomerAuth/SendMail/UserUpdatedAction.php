<?php

namespace App\Domain\Events\Actions\Customers\CustomerAuth\SendMail;

use App\Domain\Events\Mail\Customers\CustomerAuth\UserUpdated;
use Ensi\CustomerAuthClient\Api\UsersApi;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Illuminate\Support\Facades\Mail;
use RdKafka\Message;

class UserUpdatedAction
{
    public function __construct(
        private UsersApi $usersApi,
        private CustomersApi $customersApi,
    ) {
    }

    public function execute(Message $message)
    {
        $message = json_decode($message->payload);

        $user = $this->usersApi->getUser($message->user_id)->getData();

        $searchCustomersRequest = (new SearchCustomersRequest())
            ->setFilter((object)['user_id' => $message->user_id]);
        $customer = $this->customersApi->searchCustomer($searchCustomersRequest)->getData();

        $attributes = [];
        foreach ($message->old_data as $oldAttributeKey => $oldAttributeValue) {
            switch ($oldAttributeKey) {
                case 'password':
                    $attributes[$oldAttributeKey] = [
                        'old' => '*****',
                        'new' => '*****',
                    ];

                    break;
                case 'active':
                    $oldAttributeValue = $oldAttributeValue ? '1' : '0';

                    $camelAttributeName = snake_to_camel_case($oldAttributeKey, true);
                    $getterName = "get{$camelAttributeName}";
                    $newAttributeValue = $user->{$getterName}() ? '1' : '0';
                    $attributes[$oldAttributeKey] = [
                        'old' => __("messages.customers.customer_auth.user_updated.attributes.active.values.{$oldAttributeValue}"),
                        'new' => __("messages.customers.customer_auth.user_updated.attributes.active.values.{$newAttributeValue}"),
                    ];

                    break;
                default:
                    $camelAttributeName = snake_to_camel_case($oldAttributeKey, true);
                    $getterName = "get{$camelAttributeName}";
                    $attributes[$oldAttributeKey] = [
                        'old' => $oldAttributeValue,
                        'new' => $user->{$getterName}(),
                    ];
            }
        }

        Mail::to($customer->getEmail())->send(new UserUpdated([
            'subject' => __('messages.customers.customer_auth.user_updated.subject'),
            'full_name' => $customer->getFullName(),
            'attributes' => $attributes,
        ]));
    }
}
