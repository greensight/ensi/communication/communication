<?php

namespace App\Domain\Events\Actions\Customers\CustomerAuth;

use App\Domain\Events\Mail\Customers\CustomersSetPassword;
use Ensi\CustomerAuthClient\Dto\MessageForSetPassword;
use Ensi\CustomerAuthClient\ObjectSerializer;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Illuminate\Support\Facades\Mail;
use RdKafka\Message;
use Throwable;

class CustomerAuthGeneratePasswordTokenAction
{
    public function __construct(private CustomersApi $customersApi)
    {
        //
    }

    /** @throws Throwable */
    public function execute(Message $kafkaMessage)
    {
        /** @var MessageForSetPassword $message */
        $message = ObjectSerializer::deserialize($kafkaMessage->payload, MessageForSetPassword::class);
        $request = new SearchCustomersRequest([
            'filter' => [
                'user_id' => $message->getUserId(),
            ],
        ]);
        $customer = $this->customersApi->searchCustomers($request)->getData()[0];

        Mail::to($customer->getEmail())->send(new CustomersSetPassword([
            'full_name' => $customer->getFirstName() . ' ' . $customer->getLastName(),
            'link' => config('internal-services.front_urls.set_password') . $message->getToken(),
            'subject' => __('messages.customers.set_password.subject'),
        ]));
    }
}
