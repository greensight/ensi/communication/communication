<?php

namespace App\Domain\Events\Actions;

use App\Domain\Events\Mail\ManagerOrderCancelled;
use App\Domain\Events\Mail\ManagerOrderDelivered;
use App\Domain\Events\Mail\ManagerOrderPaid;
use App\Domain\Events\Mail\ManagerOrderReturned;
use App\Domain\Events\Mail\ManualText;
use App\Domain\Events\Mail\OrderCancelled;
use App\Domain\Events\Mail\OrderDelivered;
use App\Domain\Events\Mail\OrderPaid;
use App\Domain\Events\Mail\OrderReturned;
use Illuminate\Support\Facades\Mail;
use Spatie\QueueableAction\QueueableAction;

class SendEmailAction
{
    use QueueableAction;

    /** @var int Максимальное кол-во попыток (attempts) отправки одного Email */
    public int $tries = 255;

    public function execute(array $to, string $type, array $data)
    {
        switch ($type) {
            case 'plain':
                $class = ManualText::class;

                break;
            case 'order-paid':
                $class = OrderPaid::class;

                break;
            case 'order-cancelled':
                $class = OrderCancelled::class;

                break;
            case 'order-delivered':
                $class = OrderDelivered::class;

                break;
            case 'order-returned':
                $class = OrderReturned::class;

                break;
            case 'manager-order-paid':
                $class = ManagerOrderPaid::class;

                break;
            case 'manager-order-cancelled':
                $class = ManagerOrderCancelled::class;

                break;
            case 'manager-order-delivered':
                $class = ManagerOrderDelivered::class;

                break;
            case 'manager-order-returned':
                $class = ManagerOrderReturned::class;

                break;
            default:
                return;
        }

        Mail::to($to)->send(new $class($data));
    }
}
