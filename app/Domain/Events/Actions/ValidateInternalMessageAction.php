<?php

namespace App\Domain\Events\Actions;

use App\Domain\Kafka\Actions\Send\SendInternalMessageAction;
use App\Http\ApiV1\OpenApiGenerated\Enums\UserTypeEnum;
use Illuminate\Validation\Rules\Enum;

class ValidateInternalMessageAction
{
    public function __construct(private SendInternalMessageAction $sendInternalMessageAction)
    {
    }

    public function execute($request): void
    {
        $data = validator($request->get('data'), [
            'text' => ['required', 'string'],
            'chat_id' => ['required', 'integer'],
            'user_id' => ['required', 'integer'],
            'files' => ['nullable', 'array'],
            'user_type' => ['required', new Enum(UserTypeEnum::class)],
        ])->validate();

        $this->sendInternalMessageAction->execute($data);
    }
}
