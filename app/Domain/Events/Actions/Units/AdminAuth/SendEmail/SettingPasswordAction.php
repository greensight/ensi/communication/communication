<?php

namespace App\Domain\Events\Actions\Units\AdminAuth\SendEmail;

use App\Domain\Events\Mail\User\UserSetPassword;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\MessageForSetPassword;
use Ensi\AdminAuthClient\ObjectSerializer;
use Illuminate\Support\Facades\Mail;
use RdKafka\Message;
use Throwable;

class SettingPasswordAction
{
    public function __construct(private UsersApi $usersApi)
    {
        //
    }

    /** @throws Throwable */
    public function execute(Message $kafkaMessage)
    {
        /** @var MessageForSetPassword $message */
        $message = ObjectSerializer::deserialize($kafkaMessage->payload, MessageForSetPassword::class);
        $user = $this->usersApi->getUser($message->getUserId())->getData();

        Mail::to($user->getEmail())->send(new UserSetPassword([
            'full_name' => $user->getFullName(),
            'link' => config('internal-services.admin_gui_urls.set_password') . $message->getToken(),
            'subject' => __('messages.users.set_password.subject'),
        ]));
    }
}
