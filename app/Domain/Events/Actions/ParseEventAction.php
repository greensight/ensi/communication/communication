<?php

namespace App\Domain\Events\Actions;

use App\Exceptions\UndefinedEventException;
use App\Http\ApiV1\Modules\Control\Requests\ParseEventRequest;

class ParseEventAction
{
    public function __construct(
        private ValidateInternalMessageAction $validateInternalMessageAction,
        private ValidateEmailMessageAction $validateEmailMessageAction,
    ) {
    }

    public function execute(ParseEventRequest $request): void
    {
        $eventType = $request->get('type');

        match ($eventType) {
            'send-internal-message' => $this->validateInternalMessageAction->execute($request),
            'send-email-message' => $this->validateEmailMessageAction->execute($request),
            default => throw new UndefinedEventException('Undefined event', 400),
        };
    }
}
