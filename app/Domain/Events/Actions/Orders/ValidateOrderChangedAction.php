<?php

namespace App\Domain\Events\Actions\Orders;

class ValidateOrderChangedAction
{
    public function execute(array $data): array
    {
        return validator($data, [
            'to' => ['required', 'array'],
            'to.*' => ['required', 'email'],
            'subject' => ['required', 'string'],
            'order_number' => ['required', 'string'],
            'create_date' => ['required', 'string'],
            'receiver_name' => ['nullable', 'string'],
            'receiver_email' => ['nullable', 'string'],
            'receiver_phone' => ['nullable', 'string'],
            'delivery_address' => ['nullable', 'string'],
            'delivery_comment' => ['nullable', 'string'],
            'delivery_cost' => ['nullable', 'string'],
            'basket_items' => ['required', 'array'],
            'items_cost' => ['nullable', 'string'],
            'order_cost' => ['nullable', 'string'],
            'total_count' => ['nullable', 'integer'],
        ])->validate();
    }
}
