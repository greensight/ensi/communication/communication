<?php

namespace App\Domain\Events\Actions\Orders;

use App\Domain\Events\Actions\SendEmailAction;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\Orders\OrderPayload;
use App\Http\ApiV1\OpenApiGenerated\Enums\MailTypeEnum;
use Ensi\AdminAuthClient\Api\UsersApi;
use Ensi\AdminAuthClient\Dto\RequestBodyPagination as AdminAuthRequestBodyPagination;
use Ensi\AdminAuthClient\Dto\RoleEnum;
use Ensi\AdminAuthClient\Dto\SearchUsersRequest;
use Ensi\BuClient\Api\OperatorsApi;
use Ensi\BuClient\Dto\RequestBodyPagination as BuRequestBodyPagination;
use Ensi\BuClient\Dto\SearchOperatorsRequest;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\ApiException;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Exception;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

class AdminOrderMailNotify
{
    public function __construct(
        protected OrdersApi $ordersApi,
        protected OffersApi $offersApi,
        protected ProductsApi $productsApi,
        protected OperatorsApi $operatorsApi,
        protected UsersApi $usersApi,
        protected SendEmailAction $sendEmailAction,
        protected ValidateOrderChangedAction $validateOrderChangedAction
    ) {
    }

    public function payed(OrderPayload $order): void
    {
        $this->send($order, MailTypeEnum::MANAGER_ORDER_PAID, "Заказ №{$order->number} создан и оплачен");
    }

    public function cancelled(OrderPayload $order): void
    {
        $this->send($order, MailTypeEnum::MANAGER_ORDER_CANCELED, "Заказ №{$order->number} отменен");
    }

    public function delivered(OrderPayload $order): void
    {
        $this->send($order, MailTypeEnum::MANAGER_ORDER_DELIVERED, "Заказ №{$order->number} доставлен");
    }

    protected function send(OrderPayload $order, MailTypeEnum $mailType, string $subject): void
    {
        try {
            $data = array_merge($this->prepareData($order), ['subject' => $subject]);

            $validData = $this->validateOrderChangedAction->execute($data);

            $allRecipients = collect($validData['to']);

            if (count($allRecipients) > 0) {
                foreach ($allRecipients->chunk(10) as $chunk) {
                    $recipients = array_values($chunk->all());
                    $this->sendEmailAction->onQueue()->execute($recipients, $mailType->value, $validData);
                }
            }
        } catch (Exception $e) {
            Log::error($e->getMessage(), [
                'code' => $e->getCode(),
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'trace' => $e->getTraceAsString(),
            ]);

            throw $e;
        }
    }

    /**
     * @throws ApiException
     */
    protected function prepareData(OrderPayload $orderDto): array
    {
        $order = $this->ordersApi->getOrder($orderDto->id, "deliveries.shipments.orderItems")->getData();

        $orderItems = $this->getOrderItems($order);

        $totalCount = 0;
        $itemsCost = 0;
        $items = [];

        $offersIds = collect($orderItems)->pluck('offer_id')->all();
        $productsData = $this->getProductsData($offersIds);

        foreach ($orderItems as $orderItem) {
            $imageUrl = $productsData
                ->get($orderItem->getOfferId())
                ?->getMainImageFile()
                ?->getUrl();

            $items[] = [
                'name' => $orderItem->getName(),
                'image_url' => $imageUrl,
                'price' => price_format(cop2rub((int)($orderItem->getPrice() / $orderItem->getQty()))),
                'qty' => (int)$orderItem->getQty(),
                'sum' => price_format(cop2rub($orderItem->getPrice())),
            ];

            $totalCount += $orderItem->getQty();
            $itemsCost += $orderItem->getPrice();
        }

        return [
            'order_number' => $order->getNumber(),
            'create_date' => $order->getCreatedAt()->format('d.m.Y H:i'),
            'receiver_name' => $order->getReceiverName(),
            'receiver_email' => $order->getReceiverEmail(),
            'receiver_phone' => phone_print($order->getReceiverPhone()),
            'delivery_address' => $order->getDeliveryAddress()->getAddressString() ?? '',
            'delivery_comment' => $order->getDeliveryComment() ?? '',
            'delivery_cost' => price_format(cop2rub($order->getDeliveryCost())),
            'basket_items' => $items,
            'items_cost' => price_format(cop2rub($itemsCost)),
            'order_cost' => price_format(cop2rub($order->getCost())),
            'total_count' => (int)$totalCount,
            'to' => $this->getRecipientLetter($order),
        ];
    }

    protected function getRecipientLetter(Order $order): array
    {
        $searchOperatorsRequest = new SearchOperatorsRequest();
        $sellerIds = $this->getSellerIds($order);
        $searchOperatorsRequest->setFilter((object)[
            'seller_id' => $sellerIds,
        ]);
        $searchOperatorsRequest->setPagination(new BuRequestBodyPagination([
            'limit' => -1,
            'offset' => 0,
        ]));
        $operators = $this->operatorsApi->searchOperators($searchOperatorsRequest)->getData();

        $mailReceivers = [];
        $userIds = Arr::pluck($operators, 'user_id');
        if ($userIds) {
            $searchUsersRequest = new SearchUsersRequest();
            $searchUsersRequest->setFilter((object)[
                'id' => $userIds,
                'role' => RoleEnum::SELLER_ADMINISTRATOR,
            ]);
            $searchUsersRequest->setPagination(new AdminAuthRequestBodyPagination([
                'limit' => -1,
                'offset' => 0,
            ]));

            $managers = $this->usersApi->searchUsers($searchUsersRequest)->getData();

            foreach ($managers as $manager) {
                $managerEmail = $manager->getEmail() ?: $manager->getLogin();
                if ($managerEmail) {
                    $mailReceivers [] = $managerEmail;
                }
            }
        }

        return $mailReceivers;
    }

    /**
     * @param int[] $offersIds
     * @return Collection<int,Product>
     */
    protected function getProductsData(array $offersIds): Collection
    {
        if (!$offersIds) {
            return new Collection();
        }

        $offersRequest = new SearchOffersRequest();
        $offersRequest->setFilter((object)['id' => $offersIds]);

        $offers = collect($this->offersApi->searchOffers($offersRequest)->getData())
            ->mapWithKeys(fn (Offer $offer) => [$offer->getProductId() => $offer->getId()]);

        $productsRequest = new SearchProductsRequest();
        $productsRequest->setFilter((object)['id' => $offers->keys()->all()]);

        return collect($this->productsApi->searchProducts($productsRequest)->getData())
            ->mapWithKeys(fn (Product $product) => [(int)$offers->get($product->getId()) => $product]);
    }

    /**
     * @return OrderItem[]
     */
    protected function getOrderItems(Order $order): array
    {
        return collect($order->getDeliveries())
            ->pluck('shipments')->collapse()
            ->pluck('order_items')->collapse()
            ->values()->all();
    }

    /**
     * @return int[]|null
     */
    protected function getSellerIds(Order $order): ?array
    {
        return collect($order->getDeliveries())
            ->pluck('shipments')
            ->collapse()
            ->pluck('seller_id')
            ->unique()
            ->values()
            ->all();
    }
}
