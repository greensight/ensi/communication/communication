<?php

namespace App\Domain\Common\Tests\Factories\Catalog;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\OfferResponse;
use Ensi\OffersClient\Dto\SearchOffersResponse;

class OfferFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'product_id' => $this->faker->unique()->modelId(),
            'price' => $this->faker->nullable()->numberBetween(10, 100_000),
            'allow_publish' => $this->faker->boolean(),
            'is_active' => $this->faker->boolean(),
            'is_real_active' => $this->faker->boolean(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Offer
    {
        return new Offer($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): OfferResponse
    {
        return new OfferResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchOffersResponse
    {
        return $this->generateResponseSearch(SearchOffersResponse::class, $extras, $count, $pagination);
    }
}
