<?php

namespace App\Domain\Common\Tests\Factories\Orders;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\OmsClient\Dto\Shipment;
use Ensi\OmsClient\Dto\ShipmentResponse;
use Ensi\OmsClient\Dto\ShipmentStatusEnum;

class ShipmentFactory extends BaseApiFactory
{
    protected array $orderItems = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'delivery_id' => $this->faker->modelId(),
            'seller_id' => $this->faker->modelId(),
            'store_id' => $this->faker->modelId(),
            'status' => $this->faker->randomElement(ShipmentStatusEnum::getAllowableEnumValues()),
            'status_at' => $this->faker->dateTime(),
            'number' => $this->faker->unique()->numerify('######-#-#'),
            'cost' => $this->faker->numberBetween(0, 1000),
            'width' => $this->faker->randomNumber(),
            'height' => $this->faker->randomNumber(),
            'length' => $this->faker->randomNumber(),
            'weight' => $this->faker->randomNumber(),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->orderItems) {
            $definition['order_items'] = $this->orderItems;
        }

        return $definition;
    }

    public function make(array $extra = []): Shipment
    {
        return new Shipment($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): ShipmentResponse
    {
        return new ShipmentResponse(['data' => $this->make($extra)]);
    }

    public function withOrderItem(?OrderItem $orderItem = null): self
    {
        $this->orderItems[] = $orderItem ?: OrderItemFactory::new()->make();

        return $this;
    }
}
