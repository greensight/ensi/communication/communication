<?php

namespace App\Domain\Common\Tests\Factories\Orders\Data;

use App\Domain\Common\Tests\Factories\Data\BaseAddressFactory;
use Ensi\OmsClient\Dto\Address;

class DeliveryAddressFactory extends BaseAddressFactory
{
    public function make(array $extra = []): Address
    {
        return new Address($this->makeArray($extra));
    }
}
