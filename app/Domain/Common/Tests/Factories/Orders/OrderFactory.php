<?php

namespace App\Domain\Common\Tests\Factories\Orders;

use App\Domain\Common\Tests\Factories\Orders\Data\DeliveryAddressFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\DeliveryServiceEnum;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\OrderResponse;
use Ensi\OmsClient\Dto\OrderSourceEnum;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\OmsClient\Dto\PaymentMethodEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;
use Ensi\OmsClient\Dto\PaymentSystemEnum;
use Ensi\OmsClient\Dto\SearchOrdersResponse;

class OrderFactory extends BaseApiFactory
{
    protected array $deliveries = [];

    protected function definition(): array
    {
        $price = $this->faker->numberBetween(1000, 10000);
        $deliveryPrice = $this->faker->numberBetween(0, 200);

        $definition = [
            'id' => $this->faker->modelId(),
            'number' => $this->faker->unique()->numerify('######'),
            'source' => $this->faker->randomElement(OrderSourceEnum::getAllowableEnumValues()),
            'customer_id' => $this->faker->modelId(),
            'responsible_id' => $this->faker->nullable()->modelId(),
            'customer_email' => $this->faker->email(),
            'price' => $price,
            'cost' => $this->faker->numberBetween($price, $price + 10000),
            'spent_bonus' => $this->faker->numberBetween(0, 10),
            'added_bonus' => $this->faker->numberBetween(0, 10),
            'promo_code' => $this->faker->nullable()->word(),
            'delivery_method' => $this->faker->randomElement(DeliveryMethodEnum::getAllowableEnumValues()),
            'delivery_service' => $this->faker->randomElement(DeliveryServiceEnum::getAllowableEnumValues()),
            'delivery_point_id' => $this->faker->modelId(),
            'delivery_address' => DeliveryAddressFactory::new()->make(),
            'delivery_price' => $deliveryPrice,
            'delivery_cost' => $this->faker->numberBetween($deliveryPrice, $deliveryPrice + 500),
            'receiver_name' => $this->faker->name(),
            'receiver_phone' => $this->faker->numerify('+7##########'),
            'receiver_email' => $this->faker->email(),
            'status' => $this->faker->randomElement(OrderStatusEnum::getAllowableEnumValues()),
            'status_at' => $this->faker->dateTime(),
            'payment_status' => $this->faker->randomElement(PaymentStatusEnum::getAllowableEnumValues()),
            'payment_status_at' => $this->faker->dateTime(),
            'payed_at' => $this->faker->nullable()->dateTime(),
            'payment_expires_at' => $this->faker->nullable()->dateTime(),
            'payment_method' => $this->faker->randomElement(PaymentMethodEnum::getAllowableEnumValues()),
            'payment_system' => $this->faker->randomElement(PaymentSystemEnum::getAllowableEnumValues()),
            'payment_link' => $this->faker->nullable()->url(),
            'payment_external_id' => $this->faker->nullable()->uuid(),
            'is_expired' => $this->faker->boolean(),
            'is_expired_at' => $this->faker->nullable()->dateTime(),
            'is_return' => $this->faker->boolean(),
            'is_return_at' => $this->faker->nullable()->dateTime(),
            'is_partial_return' => $this->faker->boolean(),
            'is_partial_return_at' => $this->faker->nullable()->dateTime(),
            'is_problem' => $this->faker->boolean(),
            'is_problem_at' => $this->faker->nullable()->dateTime(),
            'problem_comment' => $this->faker->nullable()->text(50),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
            'delivery_comment' => $this->faker->nullable()->text(50),
            'client_comment' => $this->faker->nullable()->text(50),
        ];

        if ($this->deliveries) {
            $definition['deliveries'] = $this->deliveries;
        }

        return $definition;
    }

    public function make(array $extra = []): Order
    {
        return new Order($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): OrderResponse
    {
        return new OrderResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchOrdersResponse
    {
        return $this->generateResponseSearch(SearchOrdersResponse::class, $extras, $count, $pagination);
    }

    public function withDelivery(?Delivery $delivery = null): self
    {
        $this->deliveries[] = $delivery ?: DeliveryFactory::new()->make();

        return $this;
    }
}
