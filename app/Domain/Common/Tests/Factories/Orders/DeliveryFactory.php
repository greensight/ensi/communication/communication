<?php

namespace App\Domain\Common\Tests\Factories\Orders;

use App\Domain\Common\Tests\Factories\Orders\Data\TimeslotFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\DeliveryResponse;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Ensi\OmsClient\Dto\SearchDeliveriesResponse;
use Ensi\OmsClient\Dto\Shipment;

class DeliveryFactory extends BaseApiFactory
{
    protected array $shipments = [];

    protected function definition(): array
    {
        $definition = [
            'id' => $this->faker->modelId(),
            'order_id' => $this->faker->modelId(),
            'status' => $this->faker->randomElement(DeliveryStatusEnum::getAllowableEnumValues()),
            'status_at' => $this->faker->dateTime(),
            'number' => $this->faker->unique()->numerify('######-#'),
            'cost' => $this->faker->numberBetween(0, 1000),
            'width' => $this->faker->randomNumber(),
            'height' => $this->faker->randomNumber(),
            'length' => $this->faker->randomNumber(),
            'weight' => $this->faker->randomNumber(),
            'date' => $this->faker->dateTime(),
            'timeslot' => $this->faker->nullable()->exactly(TimeslotFactory::new()->make()),
            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];

        if ($this->shipments) {
            $definition['shipments'] = $this->shipments;
        }

        return $definition;
    }

    public function make(array $extra = []): Delivery
    {
        return new Delivery($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): DeliveryResponse
    {
        return new DeliveryResponse(['data' => $this->make($extra)]);
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchDeliveriesResponse
    {
        return $this->generateResponseSearch(SearchDeliveriesResponse::class, $extras, $count, $pagination);
    }

    public function withShipment(?Shipment $shipment = null): self
    {
        $this->shipments[] = $shipment ?: ShipmentFactory::new()->make();

        return $this;
    }
}
