<?php

namespace App\Domain\Common\Tests\Factories\Logistic\Data;

use App\Domain\Common\Tests\Factories\Data\BaseAddressFactory;
use Ensi\LogisticClient\Dto\Address;

class AddressFactory extends BaseAddressFactory
{
    public function make(array $extra = []): Address
    {
        return new Address($this->makeArray($extra));
    }
}
