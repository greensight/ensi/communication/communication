<?php

namespace App\Domain\Common\Tests\Factories\Logistic;

use App\Domain\Common\Tests\Factories\Logistic\Data\AddressFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\LogisticClient\Dto\DeliveryServiceEnum;
use Ensi\LogisticClient\Dto\Point;
use Ensi\LogisticClient\Dto\SearchPointsResponse;

class PointFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'id' => $this->faker->modelId(),

            'delivery_service_id' => $this->faker->nullable()->randomElement(DeliveryServiceEnum::getAllowableEnumValues()),
            'external_id' => $this->faker->unique()->numerify('#########'),
            'is_active' => $this->faker->boolean(),
            'name' => $this->faker->text(50),
            'description' => $this->faker->nullable()->text(50),
            'phone' => $this->faker->nullable()->numerify('+7##########'),
            'timezone' => $this->faker->nullable()->timezone(),
            'address' => $this->faker->nullable()->exactly(AddressFactory::new()->make()),
            'address_reduce' => $this->faker->nullable()->address(),
            'metro_station' => $this->faker->nullable()->text(50),
            'city_guid' => $this->faker->nullable()->uuid(),
            'geo_lat' => $this->faker->nullable()->exactly((string)$this->faker->latitude()),
            'geo_lon' => $this->faker->nullable()->exactly((string)$this->faker->longitude()),
            'only_online_payment' => $this->faker->nullable()->boolean(),
            'has_payment_card' => $this->faker->nullable()->boolean(),
            'has_courier' => $this->faker->nullable()->boolean(),
            'is_postamat' => $this->faker->nullable()->boolean(),
            'max_value' => $this->faker->nullable()->exactly((string)$this->faker->numberBetween(1, 100)),
            'max_weight' => $this->faker->numberBetween(1, 100),

            'created_at' => $this->faker->dateTime(),
            'updated_at' => $this->faker->dateTime(),
        ];
    }

    public function make(array $extra = []): Point
    {
        return new Point($this->makeArray($extra));
    }

    public function makeResponseSearch(array $extras = [], int $count = 1, mixed $pagination = null): SearchPointsResponse
    {
        return $this->generateResponseSearch(SearchPointsResponse::class, $extras, $count, $pagination);
    }
}
