<?php

namespace App\Domain\Templates\Actions\Data;

use App\Domain\Notifications\Enums\NotificationVariable;

class Variable
{
    /**
     * @param Variable[]|null $items
     */
    public function __construct(protected string $variable, protected ?array $items = null)
    {
    }

    public function isList(): bool
    {
        return $this->items !== null;
    }

    public function getList(): array
    {
        return $this->items;
    }

    public function getValue(): string
    {
        return $this->variable;
    }

    /**
     * @param NotificationVariable[] $notificationVariables
     * @return Variable[]
     */
    public static function mapFromNotificationVariable(array $notificationVariables): array
    {
        $variables = [];
        foreach ($notificationVariables as $notificationVariable) {
            $variables[] = new Variable(
                variable: $notificationVariable->data->value,
                items: $notificationVariable->isList() ? self::mapFromNotificationVariable($notificationVariable->items) : null,
            );
        }

        return $variables;
    }
}
