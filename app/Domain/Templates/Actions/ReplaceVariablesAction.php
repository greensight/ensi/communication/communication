<?php

namespace App\Domain\Templates\Actions;

use App\Domain\Templates\Actions\Data\Variable;
use App\Domain\Templates\Contracts\ValueDictionary;

class ReplaceVariablesAction
{
    /** @param Variable[] $variables */
    public function execute(string $template, array $variables, ValueDictionary $values): string
    {
        foreach ($variables as $variable) {
            while (true) {
                if (!$this->shoudReplace($template, $variable)) {
                    break;
                }

                [$variableValue, $totalValue] = $this->fillVariable($template, $variable, $values);

                $template = str_ireplace(
                    search: $variableValue,
                    replace: $totalValue,
                    subject: $template
                );
            }
        }

        return $template;
    }

    private function fillVariable(string $template, Variable $variable, ValueDictionary $values): array
    {
        if ($variable->isList()) {
            $subTemplate = $this->getSubTemplate($template, $variable->getValue());
            $subValuesItems = $values->matchList($variable->getValue());

            $filledSubTemplate = '';
            foreach ($subValuesItems as $subValues) {
                $filledSubTemplate .= $this->execute($subTemplate, $variable->getList(), $subValues);
            }

            return ["{$variable->getValue()}{$subTemplate}{$variable->getValue()}", $filledSubTemplate];
        }

        return [$variable->getValue(), $values->match($variable->getValue())];
    }

    protected function getSubTemplate(string $template, string $variable): string
    {
        return preg_match("/{$variable}(.*?){$variable}/s", $template, $matches) ? $matches[1] : '';
    }

    protected function shoudReplace(string $template, Variable $variable): bool
    {
        $countSubstr = substr_count($template, $variable->getValue());
        $minCountSubstr = $variable->isList() ? 2 : 1;

        return $countSubstr >= $minCountSubstr;
    }
}
