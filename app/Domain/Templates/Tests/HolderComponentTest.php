<?php

namespace App\Domain\Templates\Tests;

use App\Domain\Common\Tests\Factories\Customers\CustomerFactory;
use App\Domain\Common\Tests\Factories\Orders\OrderFactory;
use App\Domain\Templates\Holders\HoldersManager;
use Illuminate\Testing\Assert;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'holder');

test('Holder HoldersManager save data after load', function () {
    /** @var ComponentTestCase $this */

    $orderId = 1;
    $customerId = 1;

    $this->mockOmsOrdersApi()
        ->shouldReceive('searchOrders')
        ->andReturn(OrderFactory::new()->makeResponseSearch([['id' => $orderId]]));

    $this->mockCustomersCustomersApi()
        ->shouldReceive('searchCustomers')
        ->andReturn(CustomerFactory::new()->makeResponseSearch([['id' => $customerId]]));

    /** @var HoldersManager $holderManager */
    $holderManager = resolve(HoldersManager::class);
    $holderManager->ordersHolder->register([$orderId]);
    $holderManager->customersHolder->register([$customerId]);

    $order = $holderManager->ordersHolder->get($orderId);
    $customer = $holderManager->customersHolder->get($customerId);

    /** @var HoldersManager $otherHolderManager */
    $otherHolderManager = resolve(HoldersManager::class);
    $sameOrder = $otherHolderManager->ordersHolder->get($orderId);
    $sameCustomer = $otherHolderManager->customersHolder->get($customerId);

    Assert::assertEquals($order, $sameOrder);
    Assert::assertEquals($customer, $sameCustomer);
});
