<?php

namespace App\Domain\Templates\Tests;

use App\Domain\Common\Tests\Factories\Catalog\OfferFactory;
use App\Domain\Common\Tests\Factories\Catalog\ProductFactory;
use App\Domain\Common\Tests\Factories\Customers\CustomerFactory;
use App\Domain\Common\Tests\Factories\Logistic\PointFactory;
use App\Domain\Common\Tests\Factories\Orders\DeliveryFactory;
use App\Domain\Common\Tests\Factories\Orders\OrderFactory;
use App\Domain\Common\Tests\Factories\Orders\OrderItemFactory;
use App\Domain\Common\Tests\Factories\Orders\ShipmentFactory;
use App\Domain\Notifications\Data\NotificationVariableData;
use App\Domain\Notifications\Enums\NotificationEvent;
use App\Domain\Notifications\Enums\NotificationVariable;
use App\Domain\Templates\Actions\Data\Variable;
use App\Domain\Templates\Actions\ReplaceVariablesAction;
use App\Domain\Templates\Contracts\ValueDictionary;
use App\Domain\Templates\Data\AuthorisationValueData;
use App\Domain\Templates\Data\DeliveryValueData;
use App\Domain\Templates\Data\OrderValueData;
use App\Domain\Templates\Holders\HoldersManager;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationVariableEnum;
use Ensi\LaravelTestFactories\FakerProvider;
use Illuminate\Testing\Assert;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'notifications');

test('Action ReplaceVariablesAction simple tag', function () {
    /** @var ComponentTestCase $this */
    FakerProvider::$optionalAlways = true;

    $orderId = 1;
    $offerId = 1;
    $productId = 1;

    $orderNumber = '22222';
    $tag = NotificationVariableEnum::ORDER_NUMBER->value;

    $template = "Заказ с номером: $tag успешно создан";
    $fillTemplate = "Заказ с номером: $orderNumber успешно создан";

    $this->mockPimProductsApi()->shouldReceive('searchProducts')
        ->andReturn(ProductFactory::new()->makeResponseSearch([['id' => $productId]]));
    $this->mockOffersOffersApi()->shouldReceive('searchOffers')
        ->andReturn(OfferFactory::new()->makeResponseSearch([['id' => $offerId, 'product_id' => $productId]]));

    $orderItem = OrderItemFactory::new()->make(['offer_id' => $offerId]);
    $shipment = ShipmentFactory::new()->withOrderItem($orderItem)->make();
    $delivery = DeliveryFactory::new()->withShipment($shipment)->make();
    $ordersResponse = OrderFactory::new()->withDelivery($delivery)->makeResponseSearch([['id' => $orderId, 'number' => $orderNumber]]);

    $this->mockOmsOrdersApi()->shouldReceive('searchOrders')
        ->andReturn($ordersResponse);

    $notificationVariables = NotificationVariableData::availableVariables(current(NotificationEvent::order()));

    $variables = Variable::mapFromNotificationVariable($notificationVariables);
    $valueData = new OrderValueData($orderId, resolve(HoldersManager::class));

    /** @var ReplaceVariablesAction $action */
    $action = resolve(ReplaceVariablesAction::class);
    $result = $action->execute($template, $variables, $valueData);

    Assert::assertEquals($fillTemplate, $result);
});

test('Action ReplaceVariablesAction tag with items', function () {
    /** @var ComponentTestCase $this */
    FakerProvider::$optionalAlways = true;

    $orderId = 1;
    $offerId = 1;
    $productId = 1;
    $productName = 'Молоко';
    $productQty = 3.2;

    $tagItems = NotificationVariableEnum::ORDER_PRODUCTS->value;
    $tagName = NotificationVariableEnum::PRODUCT_NAME->value;
    $tagQty = NotificationVariableEnum::PRODUCT_QTY->value;

    $template = "Заказ доставят завтра
    <\br>Продукты<\br>
    {$tagItems}Продукт с именем: $tagName<\br>{$tagItems}
    {$tagItems}Продукт в количестве: $tagQty<\br>{$tagItems}";

    $fillTemplate = "Заказ доставят завтра
    <\br>Продукты<\br>
    Продукт с именем: $productName<\br>
    Продукт в количестве: $productQty<\br>";

    $this->mockPimProductsApi()->shouldReceive('searchProducts')
        ->andReturn(ProductFactory::new()->makeResponseSearch([['id' => $productId, 'name' => $productName]]));
    $this->mockOffersOffersApi()->shouldReceive('searchOffers')
        ->andReturn(OfferFactory::new()->makeResponseSearch([['id' => $offerId, 'product_id' => $productId]]));

    $orderItem = OrderItemFactory::new()->make(['offer_id' => $offerId, 'qty' => $productQty]);
    $shipment = ShipmentFactory::new()->withOrderItem($orderItem)->make();
    $delivery = DeliveryFactory::new()->withShipment($shipment)->make();
    $ordersResponse = OrderFactory::new()->withDelivery($delivery)->makeResponseSearch([['id' => $orderId]]);

    $this->mockOmsOrdersApi()->shouldReceive('searchOrders')
        ->andReturn($ordersResponse);

    $notificationVariables = NotificationVariableData::availableVariables(current(NotificationEvent::order()));

    $variables = Variable::mapFromNotificationVariable($notificationVariables);
    $valueData = new OrderValueData($orderId, resolve(HoldersManager::class));

    /** @var ReplaceVariablesAction $action */
    $action = resolve(ReplaceVariablesAction::class);
    $result = $action->execute($template, $variables, $valueData);

    Assert::assertEquals($fillTemplate, $result);
});

test('Action ReplaceVariablesAction finish with incorrect tag', function () {
    /** @var ComponentTestCase $this */
    FakerProvider::$optionalAlways = true;

    $orderId = 1;
    $offerId = 1;
    $productId = 1;
    $productName = 'Молоко';
    $productQty = 3.2;

    $tagItems = NotificationVariableEnum::ORDER_PRODUCTS->value;
    $tagQty = NotificationVariableEnum::PRODUCT_QTY->value;

    $template = "{$tagItems}Продукт в количестве: $tagQty<\br>";
    $fillTemplate = "{$tagItems}Продукт в количестве: $tagQty<\br>";

    $this->mockPimProductsApi()->shouldReceive('searchProducts')
        ->andReturn(ProductFactory::new()->makeResponseSearch([['id' => $productId, 'name' => $productName]]));
    $this->mockOffersOffersApi()->shouldReceive('searchOffers')
        ->andReturn(OfferFactory::new()->makeResponseSearch([['id' => $offerId, 'product_id' => $productId]]));

    $orderItem = OrderItemFactory::new()->make(['offer_id' => $offerId, 'qty' => $productQty]);
    $shipment = ShipmentFactory::new()->withOrderItem($orderItem)->make();
    $delivery = DeliveryFactory::new()->withShipment($shipment)->make();
    $ordersResponse = OrderFactory::new()->withDelivery($delivery)->makeResponseSearch([['id' => $orderId]]);

    $this->mockOmsOrdersApi()->shouldReceive('searchOrders')
        ->andReturn($ordersResponse);

    $notificationVariables = NotificationVariableData::availableVariables(current(NotificationEvent::order()));

    $variables = Variable::mapFromNotificationVariable($notificationVariables);
    $valueData = new OrderValueData($orderId, resolve(HoldersManager::class));

    /** @var ReplaceVariablesAction $action */
    $action = resolve(ReplaceVariablesAction::class);
    $result = $action->execute($template, $variables, $valueData);

    Assert::assertEquals($fillTemplate, $result);
});

/**
 * @param NotificationVariable[] $enums
 */
function checkItemsMath(array $enums, ValueDictionary $valueDictionary): void
{
    foreach ($enums as $enum) {
        if ($enum->isList()) {
            checkItemsMath($enum->items, current($valueDictionary->matchList($enum->data->value)));
        } else {
            Assert::assertNotNull($valueDictionary->match($enum->data->value));
        }
    }
}

test('Data checking for all matches for AuthorisationValueData', function (array $variables) {
    /** @var ComponentTestCase $this */
    FakerProvider::$optionalAlways = true;

    $userId = 1;

    $this->mockCustomersCustomersApi()->shouldReceive('searchCustomers')
        ->andReturn(CustomerFactory::new()->makeResponseSearch([['user_id' => $userId]]));

    /** @var HoldersManager $holderManager */
    $holderManager = resolve(HoldersManager::class);
    $holderManager->confirmationCodesHolder->setValue($userId, 'code');

    $valueData = new AuthorisationValueData($userId, resolve(HoldersManager::class));

    checkItemsMath($variables, $valueData);
})->with(['authorisation' => [NotificationVariable::authorisation()]]);

test('Data checking for all matches for OrderValueData', function (array $variables) {
    /** @var ComponentTestCase $this */
    FakerProvider::$optionalAlways = true;

    $orderId = 10;
    $customerId = 10;
    $offerId = 10;
    $productId = 10;
    $pointId = 10;

    $this->mockPimProductsApi()->shouldReceive('searchProducts')
        ->andReturn(ProductFactory::new()->makeResponseSearch([['id' => $productId]]));
    $this->mockOffersOffersApi()->shouldReceive('searchOffers')
        ->andReturn(OfferFactory::new()->makeResponseSearch([['id' => $offerId, 'product_id' => $productId]]));
    $this->mockCustomersCustomersApi()->shouldReceive('searchCustomers')
        ->andReturn(CustomerFactory::new()->makeResponseSearch([['id' => $customerId]]));
    $this->mockLogisticGeosApi()->shouldReceive('searchPoints')
        ->andReturn(PointFactory::new()->makeResponseSearch([['id' => $pointId]]));

    $orderItem = OrderItemFactory::new()->make(['offer_id' => $offerId]);
    $shipment = ShipmentFactory::new()->withOrderItem($orderItem)->make();
    $delivery = DeliveryFactory::new()->withShipment($shipment)->make();
    $ordersResponse = OrderFactory::new()
        ->withDelivery($delivery)
        ->makeResponseSearch([['id' => $orderId, 'customer_id' => $customerId, 'delivery_point_id' => $pointId]]);

    $this->mockOmsOrdersApi()
        ->shouldReceive('searchOrders')
        ->andReturn($ordersResponse);

    $valueData = new OrderValueData($orderId, resolve(HoldersManager::class));

    checkItemsMath($variables, $valueData);
})->with(['order' => [NotificationVariable::order()]]);

test('Data checking for all matches for DeliveryValueData', function (array $variables) {
    /** @var ComponentTestCase $this */
    FakerProvider::$optionalAlways = true;

    $deliveryId = 10;
    $orderId = 10;
    $customerId = 10;
    $offerId = 10;
    $productId = 10;
    $pointId = 10;

    $this->mockPimProductsApi()->shouldReceive('searchProducts')
        ->andReturn(ProductFactory::new()->makeResponseSearch([['id' => $productId]]));
    $this->mockOffersOffersApi()->shouldReceive('searchOffers')
        ->andReturn(OfferFactory::new()->makeResponseSearch([['id' => $offerId, 'product_id' => $productId]]));
    $this->mockCustomersCustomersApi()->shouldReceive('searchCustomers')
        ->andReturn(CustomerFactory::new()->makeResponseSearch([['id' => $customerId]]));
    $this->mockLogisticGeosApi()->shouldReceive('searchPoints')
        ->andReturn(PointFactory::new()->makeResponseSearch([['id' => $pointId]]));
    $this->mockOmsOrdersApi()->shouldReceive('searchOrders')
        ->andReturn(OrderFactory::new()
            ->makeResponseSearch([['id' => $orderId, 'customer_id' => $customerId, 'delivery_point_id' => $pointId]]));

    $orderItem = OrderItemFactory::new()->make(['offer_id' => $offerId]);
    $shipment = ShipmentFactory::new()->withOrderItem($orderItem)->make();
    $delivery = DeliveryFactory::new()->withShipment($shipment)->makeResponseSearch([['id' => $deliveryId, 'order_id' => $orderId]]);

    $this->mockOmsDeliveriesApi()->shouldReceive('searchDeliveries')
        ->andReturn($delivery);

    $valueData = new DeliveryValueData($deliveryId, resolve(HoldersManager::class));

    checkItemsMath($variables, $valueData);
})->with(['delivery' => [NotificationVariable::delivery()]]);
