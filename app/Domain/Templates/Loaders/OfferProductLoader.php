<?php

namespace App\Domain\Templates\Loaders;

use App\Domain\Templates\Contracts\Loader;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OffersClient\Dto\Offer;
use Ensi\OffersClient\Dto\SearchOffersRequest;
use Ensi\PimClient\Api\ProductsApi;
use Ensi\PimClient\Dto\Product;
use Ensi\PimClient\Dto\SearchProductsRequest;
use Illuminate\Support\Collection;

class OfferProductLoader implements Loader
{
    public function __construct(
        protected readonly ProductsApi $productsApi,
        protected readonly OffersApi $offersApi,
    ) {
    }

    /** @return Collection<int, Product> */
    public function load(array $ids): Collection
    {
        $offerRequest = new SearchOffersRequest();
        $offerRequest->setFilter((object) ['id' => $ids]);

        $offers = collect($this->offersApi->searchOffers($offerRequest)->getData())
            ->keyBy(fn (Offer $offer) => $offer->getProductId());

        $productRequest = new SearchProductsRequest();
        $productRequest->setFilter((object) ['id' => $offers->keys()->all()]);

        return collect($this->productsApi->searchProducts($productRequest)->getData())
            ->keyBy(fn (Product $product) => $offers->get($product->getId())->getId());
    }
}
