<?php

namespace App\Domain\Templates\Loaders;

use App\Domain\Templates\Contracts\Loader;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\SearchOrdersRequest;
use Illuminate\Support\Collection;

class OrderLoader implements Loader
{
    public function __construct(protected readonly OrdersApi $api)
    {
    }

    /** @return Collection<int, Order> */
    public function load(array $ids): Collection
    {
        $request = new SearchOrdersRequest();
        $request->setFilter((object) ['id' => $ids]);
        $request->setInclude(['deliveries.shipments.orderItems']);

        return collect($this->api->searchOrders($request)->getData())->keyBy(fn (Order $order) => $order->getId());
    }
}
