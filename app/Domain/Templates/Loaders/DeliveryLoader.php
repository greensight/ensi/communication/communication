<?php

namespace App\Domain\Templates\Loaders;

use App\Domain\Templates\Contracts\Loader;
use Ensi\OmsClient\Api\DeliveriesApi;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\SearchDeliveriesRequest;
use Illuminate\Support\Collection;

class DeliveryLoader implements Loader
{
    public function __construct(protected readonly DeliveriesApi $api)
    {
    }

    /** @return Collection<int, Delivery> */
    public function load(array $ids): Collection
    {
        $request = new SearchDeliveriesRequest();
        $request->setFilter((object) ['id' => $ids]);
        $request->setInclude(['shipments.orderItems']);

        return collect($this->api->searchDeliveries($request)->getData())->keyBy(fn (Delivery $delivery) => $delivery->getId());
    }
}
