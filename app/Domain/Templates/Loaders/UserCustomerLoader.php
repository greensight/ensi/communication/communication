<?php

namespace App\Domain\Templates\Loaders;

use App\Domain\Templates\Contracts\Loader;
use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\CustomersClient\Dto\Customer;
use Ensi\CustomersClient\Dto\SearchCustomersRequest;
use Illuminate\Support\Collection;

class UserCustomerLoader implements Loader
{
    public function __construct(protected readonly CustomersApi $api)
    {
    }

    /** @return Collection<int, Customer> */
    public function load(array $ids): Collection
    {
        $request = new SearchCustomersRequest();
        $request->setFilter((object)['user_id' => $ids]);

        return collect($this->api->searchCustomers($request)->getData())->keyBy(fn (Customer $customer) => $customer->getUserId());
    }
}
