<?php

namespace App\Domain\Templates\Loaders;

use App\Domain\Templates\Contracts\Loader;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\Dto\Point;
use Ensi\LogisticClient\Dto\SearchPointsRequest;
use Illuminate\Support\Collection;

class PointLoader implements Loader
{
    public function __construct(protected readonly GeosApi $api)
    {
    }

    /** @return Collection<int, Point> */
    public function load(array $ids): Collection
    {
        $request = new SearchPointsRequest();
        $request->setFilter((object) ['id' => $ids]);

        return collect($this->api->searchPoints($request)->getData())->keyBy(fn (Point $point) => $point->getId());

    }
}
