<?php

namespace App\Domain\Templates\Contracts;

interface ValueDictionary
{
    public function match(string $variable): ?string;

    /** @return ValueDictionary[] */
    public function matchList(string $variable): array;
}
