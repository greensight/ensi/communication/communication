<?php

namespace App\Domain\Templates\Contracts;

use Illuminate\Support\Collection;

interface Loader
{
    public function load(array $ids): Collection;
}
