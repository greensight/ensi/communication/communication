<?php

namespace App\Domain\Templates\Holders;

use App\Domain\Templates\Contracts\Loader;
use Illuminate\Support\Collection;

/**
 * @template T
 */
abstract class BaseCollectionHolder
{
    /** @var Collection<int, T>|null */
    protected ?Collection $elements = null;

    public function __construct(protected Loader $loader)
    {
        $this->elements = collect();
    }

    protected function has(int $id): bool
    {
        return $this->elements->has($id);
    }

    /** @return T */
    protected function getValue(int $id): mixed
    {
        return $this->elements->get($id);
    }

    /** @param Collection<int, T> $items */
    public function setValues(Collection $items): void
    {
        $this->elements = $this->elements->union($items);
    }

    public function register(array $ids): void
    {
        $uploadIds = [];
        foreach ($ids as $id) {
            if (!$this->has($id)) {
                $uploadIds[] = $id;
            }
        }

        if (filled($uploadIds)) {
            $this->setValues($this->loader->load($uploadIds));
        }
    }

    /** @return T */
    public function get(int $id): mixed
    {
        $this->register([$id]);

        return $this->getValue($id);
    }

    /** @return T[] */
    public function getFew(array $ids): array
    {
        $this->register($ids);

        $products = [];
        foreach ($ids as $id) {
            $products = $this->getValue($id);
        }

        return $products;
    }
}
