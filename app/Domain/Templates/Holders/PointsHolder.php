<?php

namespace App\Domain\Templates\Holders;

use App\Domain\Templates\Loaders\PointLoader;
use Ensi\LogisticClient\Dto\Point;
use Illuminate\Support\Collection;

/**
 * @extends BaseCollectionHolder<Point>
 */
class PointsHolder extends BaseCollectionHolder
{
    public function __construct(PointLoader $loader)
    {
        parent::__construct($loader);
        $this->points = collect();
    }

    /** @var Collection<int, Point>|null */
    protected ?Collection $points = null;

    protected function has(int $id): bool
    {
        return $this->points->has($id);
    }

    protected function getValue(int $id): Point
    {
        return $this->points->get($id);
    }

    public function setValues(Collection $items): void
    {
        $this->points = $this->points->union($items);
    }
}
