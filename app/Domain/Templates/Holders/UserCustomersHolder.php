<?php

namespace App\Domain\Templates\Holders;

use App\Domain\Templates\Loaders\UserCustomerLoader;
use Ensi\CustomersClient\Dto\Customer;
use Illuminate\Support\Collection;

/**
 * @extends BaseCollectionHolder<Customer>
 */
class UserCustomersHolder extends BaseCollectionHolder
{
    public function __construct(UserCustomerLoader $loader)
    {
        parent::__construct($loader);
        if (self::$customers === null) {
            self::$customers = collect();
        }
    }

    /** @var Collection<int, Customer>|null */
    protected static ?Collection $customers = null;

    protected function has(int $id): bool
    {
        return self::$customers->has($id);
    }

    protected function getValue(int $id): Customer
    {
        return self::$customers->get($id);
    }

    public function setValues(Collection $items): void
    {
        self::$customers = self::$customers->union($items);
    }
}
