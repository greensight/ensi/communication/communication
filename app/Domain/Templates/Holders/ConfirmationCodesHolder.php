<?php

namespace App\Domain\Templates\Holders;

use Illuminate\Support\Collection;

class ConfirmationCodesHolder
{
    public function __construct()
    {
        $this->confirmationCodes = collect();
    }

    /** @var Collection<int, string>|null */
    protected ?Collection $confirmationCodes = null;

    public function getValue(int $userId): ?string
    {
        return $this->confirmationCodes->get($userId);
    }

    public function setValue(int $userId, string $confirmationCode): void
    {
        $this->confirmationCodes->put($userId, $confirmationCode);
    }
}
