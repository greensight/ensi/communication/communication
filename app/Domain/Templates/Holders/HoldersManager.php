<?php

namespace App\Domain\Templates\Holders;

use App\Domain\Templates\Loaders\CustomerLoader;
use App\Domain\Templates\Loaders\DeliveryLoader;
use App\Domain\Templates\Loaders\OfferProductLoader;
use App\Domain\Templates\Loaders\OrderLoader;
use App\Domain\Templates\Loaders\PointLoader;
use App\Domain\Templates\Loaders\UserCustomerLoader;
use Illuminate\Contracts\Foundation\Application;

class HoldersManager
{
    public function __construct(
        public readonly OfferProductsHolder $offerProductsHolder,
        public readonly OrdersHolder $ordersHolder,
        public readonly DeliveriesHolder $deliveriesHolder,
        public readonly PointsHolder $pointsHolder,
        public readonly CustomersHolder $customersHolder,
        public readonly UserCustomersHolder $userCustomersHolder,
        public readonly ConfirmationCodesHolder $confirmationCodesHolder,
    ) {
    }

    public static function registerInApp(Application $app): void
    {
        $app->singleton(ConfirmationCodesHolder::class, fn (Application $app) => new ConfirmationCodesHolder());
        $app->singleton(CustomersHolder::class, fn (Application $app) => new CustomersHolder($app->make(CustomerLoader::class)));
        $app->singleton(DeliveriesHolder::class, fn (Application $app) => new DeliveriesHolder($app->make(DeliveryLoader::class)));
        $app->singleton(OfferProductsHolder::class, fn (Application $app) => new OfferProductsHolder($app->make(OfferProductLoader::class)));
        $app->singleton(OrdersHolder::class, fn (Application $app) => new OrdersHolder($app->make(OrderLoader::class)));
        $app->singleton(PointsHolder::class, fn (Application $app) => new PointsHolder($app->make(PointLoader::class)));
        $app->singleton(UserCustomersHolder::class, fn (Application $app) => new UserCustomersHolder($app->make(UserCustomerLoader::class)));
    }
}
