<?php

namespace App\Domain\Templates\Data\Items;

use App\Domain\Templates\Contracts\ValueDictionary;
use App\Domain\Templates\Holders\HoldersManager;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationVariableEnum;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\OrderItem;
use Exception;
use Illuminate\Support\Collection;

class DeliveryItemValueData implements ValueDictionary
{
    public function __construct(protected readonly Delivery $deliveryItem, protected readonly HoldersManager $holdersManager)
    {
    }

    public function match(string $variable): ?string
    {
        return match ($variable) {
            NotificationVariableEnum::ORDER_DELIVERY_NUMBER->value => $this->deliveryItem->getNumber(),
            NotificationVariableEnum::ORDER_DELIVERY_DATE->value => $this->deliveryItem->getDate()->format('d.m.Y'),
            NotificationVariableEnum::ORDER_DELIVERY_TIMESLOT->value => "{$this->deliveryItem->getTimeslot()->getFrom()} - {$this->deliveryItem->getTimeslot()->getTo()}",
            default => throw new Exception('Unexpected match value'),
        };
    }

    public function matchList(string $variable): array
    {
        return match ($variable) {
            NotificationVariableEnum::DELIVERY_PRODUCTS->value => $this->itemsValues(),
            default => throw new Exception('Unexpected match value'),
        };
    }

    protected function itemsValues(): array
    {
        $this->holdersManager->offerProductsHolder->register($this->items()->pluck('offer_id')->all());

        return $this->items()->map(fn (OrderItem $item) => new OrderItemValueData($item, $this->holdersManager))->all();
    }

    protected function items(): Collection
    {
        return collect($this->deliveryItem->getShipments())
            ->pluck('order_items')->collapse();
    }
}
