<?php

namespace App\Domain\Templates\Data\Items;

use App\Domain\Templates\Contracts\ValueDictionary;
use App\Domain\Templates\Holders\HoldersManager;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationVariableEnum;
use Ensi\OmsClient\Dto\OrderItem;
use Ensi\PimClient\Dto\Product;
use Exception;

class OrderItemValueData implements ValueDictionary
{
    public function __construct(protected readonly OrderItem $orderItem, protected readonly HoldersManager $holdersManager)
    {
    }

    public function match(string $variable): ?string
    {
        return match ($variable) {
            NotificationVariableEnum::PRODUCT_NAME->value => $this->offerProduct()->getName(),
            NotificationVariableEnum::PRODUCT_QTY->value => $this->orderItem->getQty(),
            NotificationVariableEnum::PRODUCT_PRICE->value => $this->orderItem->getPricePerOne(),
            NotificationVariableEnum::PRODUCT_COST->value => $this->orderItem->getCostPerOne(),
            NotificationVariableEnum::PRODUCT_IMAGE->value => $this->offerProduct()->getMainImageFile()?->getUrl(),
            default => throw new Exception('Unexpected match value'),
        };
    }

    public function matchList(string $variable): array
    {
        return [];
    }

    protected function offerProduct(): Product
    {
        return $this->holdersManager->offerProductsHolder->get($this->orderItem->getOfferId());
    }
}
