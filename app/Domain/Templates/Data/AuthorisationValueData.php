<?php

namespace App\Domain\Templates\Data;

use App\Domain\Templates\Contracts\ValueDictionary;
use App\Domain\Templates\Holders\HoldersManager;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationVariableEnum;
use Ensi\CustomersClient\Dto\Customer;
use Exception;

class AuthorisationValueData implements ValueDictionary
{
    public function __construct(protected int $userId, protected readonly HoldersManager $holdersManager)
    {
    }

    public function match(string $variable): ?string
    {
        return match ($variable) {
            NotificationVariableEnum::CUSTOMER_NAME->value => $this->customer()->getFullName(),
            NotificationVariableEnum::CONFIRMATION_CODE->value => $this->confirmationCode(),
            default => throw new Exception('Unexpected match value'),
        };
    }

    public function matchList(string $variable): array
    {
        return [];
    }

    protected function customer(): Customer
    {
        return $this->holdersManager->userCustomersHolder->get($this->userId);
    }

    protected function confirmationCode(): string
    {
        return $this->holdersManager->confirmationCodesHolder->getValue($this->userId);
    }
}
