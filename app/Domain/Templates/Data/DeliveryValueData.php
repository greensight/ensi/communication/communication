<?php

namespace App\Domain\Templates\Data;

use App\Domain\Templates\Contracts\ValueDictionary;
use App\Domain\Templates\Data\Items\OrderItemValueData;
use App\Domain\Templates\Holders\HoldersManager;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationVariableEnum;
use Ensi\CustomersClient\Dto\Customer;
use Ensi\LogisticClient\Dto\DeliveryMethodEnum;
use Ensi\LogisticClient\Dto\Point;
use Ensi\OmsClient\Dto\Delivery;
use Ensi\OmsClient\Dto\Order;
use Ensi\OmsClient\Dto\OrderItem;
use Exception;
use Illuminate\Support\Collection;

class DeliveryValueData implements ValueDictionary
{
    public function __construct(protected int $deliveryId, protected readonly HoldersManager $holdersManager)
    {
    }

    public function match(string $variable): ?string
    {
        return match ($variable) {
            NotificationVariableEnum::ORDER_RECEIVER_NAME->value => $this->order()->getReceiverName(),
            NotificationVariableEnum::ORDER_RECEIVER_PHONE->value => $this->order()->getReceiverPhone(),
            NotificationVariableEnum::ORDER_NUMBER->value => $this->order()->getNumber(),
            NotificationVariableEnum::ORDER_PAYMENT_LINK->value => $this->order()->getPaymentLink(),
            NotificationVariableEnum::ORDER_PRICE->value => $this->order()->getPrice(),
            NotificationVariableEnum::ORDER_PRODUCTS_PRICE->value => $this->order()->getPrice() - $this->order()->getDeliveryPrice(),
            NotificationVariableEnum::ORDER_DELIVERY_PRICE->value => $this->order()->getDeliveryPrice(),
            NotificationVariableEnum::ORDER_DELIVERY_METHOD->value => DeliveryMethodEnum::getDescriptions()[$this->order()->getDeliveryMethod()] ?? null,
            NotificationVariableEnum::ORDER_DELIVERY_ADDRESS->value => $this->order()->getDeliveryAddress()?->getAddressString(),
            NotificationVariableEnum::ORDER_DELIVERY_POINT->value => "{$this->point()?->getName()} {$this->point()?->getAddress()?->getAddressString()}",
            NotificationVariableEnum::ORDER_DELIVERY_COMMENT->value => $this->order()->getDeliveryComment(),
            NotificationVariableEnum::ORDER_DELIVERY_NUMBER->value => $this->delivery()->getNumber(),
            NotificationVariableEnum::ORDER_DELIVERY_DATE->value => $this->delivery()->getDate()->format('d.m.Y'),
            NotificationVariableEnum::ORDER_DELIVERY_TIMESLOT->value => "{$this->delivery()->getTimeslot()->getFrom()} - {$this->delivery()->getTimeslot()->getTo()}",
            NotificationVariableEnum::CUSTOMER_NAME->value => $this->customer()->getFullName(),
            default => throw new Exception('Unexpected match value'),
        };
    }

    public function matchList(string $variable): array
    {
        return match ($variable) {
            NotificationVariableEnum::DELIVERY_PRODUCTS->value => $this->itemsValues(),
            default => throw new Exception('Unexpected match value'),
        };
    }

    protected function itemsValues(): array
    {
        $this->holdersManager->offerProductsHolder->register($this->items()->pluck('offer_id')->all());

        return $this->items()->map(fn (OrderItem $item) => new OrderItemValueData($item, $this->holdersManager))->all();
    }

    protected function delivery(): Delivery
    {
        return $this->holdersManager->deliveriesHolder->get($this->deliveryId);
    }

    protected function order(): Order
    {
        return $this->holdersManager->ordersHolder->get($this->delivery()->getOrderId());
    }

    protected function customer(): Customer
    {
        return $this->holdersManager->customersHolder->get($this->order()->getCustomerId());
    }

    protected function point(): ?Point
    {
        $pointId = $this->order()->getDeliveryPointId();
        if ($pointId === null) {
            return null;
        }

        return $this->holdersManager->pointsHolder->get($pointId);
    }

    protected function items(): Collection
    {
        return collect($this->delivery()->getShipments())
            ->pluck('order_items')->collapse();
    }
}
