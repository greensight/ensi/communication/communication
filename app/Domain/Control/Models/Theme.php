<?php

namespace App\Domain\Control\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Theme
 *
 * @property int $id
 * @property string $name
 * @property bool $active
 * @property array $channel
 */
class Theme extends Model
{
    protected $table = 'themes';

    protected $casts = [
        'channel' => 'array',
    ];

    protected $fillable = [
        'name', 'active', 'channel',
    ];
}
