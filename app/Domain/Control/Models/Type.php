<?php

namespace App\Domain\Control\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Theme
 *
 * @property int $id
 * @property string $name
 * @property bool $active
 * @property array $channel
 */
class Type extends Model
{
    protected $table = 'types';

    protected $casts = [
        'channel' => 'array',
    ];

    protected $fillable = [
        'name', 'active', 'channel',
    ];
}
