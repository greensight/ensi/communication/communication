<?php

namespace App\Domain\Control\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Status
 *
 * @property int $id
 * @property string $name
 * @property bool $active
 * @property bool $default
 * @property array $channel
 */
class Status extends Model
{
    protected $table = 'statuses';

    protected $casts = [
        'channel' => 'array',
    ];

    protected $fillable = [
        'name', 'active', 'default', 'channel',
    ];
}
