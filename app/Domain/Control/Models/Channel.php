<?php

namespace App\Domain\Control\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Channel
 *
 * @property int $id
 * @property string $name
 */
class Channel extends Model
{
    protected $table = 'channels';

    protected $fillable = ['name'];
}
