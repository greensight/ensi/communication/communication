<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Type;

class PatchTypeAction
{
    public function execute(int $typeId, array $fields): Type
    {
        /** @var Type $type */
        $type = Type::query()->findOrFail($typeId);
        $type->update($fields);

        return $type;
    }
}
