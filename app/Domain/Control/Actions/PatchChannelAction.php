<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Channel;

class PatchChannelAction
{
    public function execute(int $id, array $fields): Channel
    {
        /** @var Channel $channel */
        $channel = Channel::query()->findOrFail($id);
        $channel->update($fields);

        return $channel;
    }
}
