<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Theme;

class CreateThemeAction
{
    public function execute(array $fields): Theme
    {
        $theme = new Theme();
        $theme->fill($fields);
        $theme->save();

        return $theme;
    }
}
