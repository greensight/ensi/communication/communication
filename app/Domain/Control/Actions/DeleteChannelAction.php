<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Channel;

class DeleteChannelAction
{
    public function execute(int $id): void
    {
        Channel::destroy($id);
    }
}
