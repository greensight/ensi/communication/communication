<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Channel;

class CreateChannelAction
{
    public function execute(array $fields): Channel
    {
        $channel = new Channel();
        $channel->fill($fields);
        $channel->save();

        return $channel;
    }
}
