<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Type;

class CreateTypeAction
{
    public function execute(array $fields): Type
    {
        $type = new Type();
        $type->fill($fields);
        $type->save();

        return $type;
    }
}
