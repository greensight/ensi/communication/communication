<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Theme;

class PatchThemeAction
{
    public function execute(int $id, array $fields): Theme
    {
        /** @var Theme $theme */
        $theme = Theme::query()->findOrFail($id);
        $theme->update($fields);

        return $theme;
    }
}
