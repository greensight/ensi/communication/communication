<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Status;

class PatchStatusAction
{
    public function execute(int $id, array $fields): Status
    {
        /** @var Status $status */
        $status = Status::query()->findOrFail($id);
        $status->update($fields);

        return $status;
    }
}
