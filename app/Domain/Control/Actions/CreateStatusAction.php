<?php

namespace App\Domain\Control\Actions;

use App\Domain\Control\Models\Status;

class CreateStatusAction
{
    public function execute(array $fields): Status
    {
        $status = new Status();
        $status->fill($fields);
        $status->save();

        return $status;
    }
}
