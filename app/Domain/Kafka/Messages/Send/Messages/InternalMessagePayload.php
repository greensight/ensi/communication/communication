<?php

namespace App\Domain\Kafka\Messages\Send\Messages;

use App\Domain\Kafka\Messages\Send\Payload;

class InternalMessagePayload extends Payload
{
    public function __construct(
        public string $text,
        public int $chatId,
        public int $userId,
        public int $userType,
        public ?array $files = null,
    ) {
    }

    public static function make(array $data): self
    {
        return new self(
            text: $data['text'],
            chatId: $data['chat_id'],
            userId: $data['user_id'],
            userType: $data['user_type'],
            files: $data['files'] ?? null,
        );
    }

    public function jsonSerialize(): array
    {
        return array_filter([
            'text' => $this->text,
            'chat_id' => $this->chatId,
            'user_id' => $this->userId,
            'user_type' => $this->userType,
            'files' => $this->files,
        ]);
    }
}
