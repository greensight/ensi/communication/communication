<?php

namespace App\Domain\Kafka\Messages\Send\Messages;

use App\Domain\Kafka\Messages\Send\KafkaMessage;

class InternalMessage extends KafkaMessage
{
    public function __construct(public InternalMessagePayload $payload)
    {
    }

    public function toArray(): array
    {
        return $this->payload->jsonSerialize();
    }

    public function topicKey(): string
    {
        return 'create-message';
    }
}
