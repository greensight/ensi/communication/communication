<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Units\AdminUsers;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\AdminUsers\AdminUserPayloadFactory;

/**
 * @property int $id - идентификатор пользователя
 * @property bool $active - активность пользователя
 * @property string|null $cause_deactivation - причина деактивации
 * @property string $full_name - ФИО
 * @property string|null $email - email
 * @property string|null $phone - номер телефона
 * @property string|null $password_token - токен для обновления пароля
 */
class AdminUserPayload extends Payload
{
    public static function factory(): AdminUserPayloadFactory
    {
        return AdminUserPayloadFactory::new();
    }
}
