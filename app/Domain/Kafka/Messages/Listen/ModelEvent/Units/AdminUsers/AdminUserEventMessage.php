<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Units\AdminUsers;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\AdminUsers\AdminUserEventMessageFactory;

class AdminUserEventMessage extends ModelEventMessage
{
    public AdminUserPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new AdminUserPayload($attributes);
    }

    public static function factory(): AdminUserEventMessageFactory
    {
        return AdminUserEventMessageFactory::new();
    }
}
