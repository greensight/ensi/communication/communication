<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\AdminUsers;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\ModelEventMessageFactory;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;

class DeliveryEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return [
            'id' => $this->faker->modelId(),
            'status' => $this->faker->randomElement(DeliveryStatusEnum::getAllowableEnumValues()),
        ];
    }
}
