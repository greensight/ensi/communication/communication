<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\AdminUsers;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Units\AdminUsers\AdminUserPayload;
use Ensi\LaravelTestFactories\Factory;

class AdminUserPayloadFactory extends Factory
{
    protected bool $withEmail = false;

    protected function definition(): array
    {
        $email = $this->withEmail ? $this->faker->email() : $this->faker->optional()->email();

        return [
            'id' => $this->faker->numberBetween(1),
            'active' => $this->faker->boolean(),
            'full_name' => "{$this->faker->firstName()} {$this->faker->lastName()} {$this->faker->firstName()}",
            'phone' => $this->faker->optional()->phoneNumber(),
            'email' => $email,
            'password_token' => $this->faker->optional()->text(50),
        ];
    }

    public function withEmail(): self
    {
        $this->withEmail = true;

        return $this;
    }

    public function make(array $extra = []): AdminUserPayload
    {
        return new AdminUserPayload($this->makeArray($extra));
    }
}
