<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\AdminUsers;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\ModelEventMessageFactory;

class AdminUserEventMessageFactory extends ModelEventMessageFactory
{
    protected function definitionAttributes(): array
    {
        return AdminUserPayloadFactory::new()->makeArray();
    }
}
