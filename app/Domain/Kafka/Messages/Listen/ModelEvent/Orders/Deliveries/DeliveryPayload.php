<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\Deliveries;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;

/**
 * @property int $id - id отправления
 * @property int $status - статус из DeliveryStatusEnum
 */
class DeliveryPayload extends Payload
{
}
