<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\Deliveries;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\AdminUsers\DeliveryEventMessageFactory;

class DeliveryEventMessage extends ModelEventMessage
{
    public DeliveryPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new DeliveryPayload($attributes);
    }

    public static function factory(): DeliveryEventMessageFactory
    {
        return DeliveryEventMessageFactory::new();
    }
}
