<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\Orders;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Tests\Factories\AdminUsers\OrderEventMessageFactory;

class OrderEventMessage extends ModelEventMessage
{
    public OrderPayload $attributes;

    public function setAttributes(array $attributes): void
    {
        $this->attributes = new OrderPayload($attributes);
    }

    public static function factory(): OrderEventMessageFactory
    {
        return OrderEventMessageFactory::new();
    }
}
