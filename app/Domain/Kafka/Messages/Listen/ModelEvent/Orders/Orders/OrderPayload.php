<?php

namespace App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\Orders;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;

/**
 * @property int $id id заказа
 * @property string $number номер заказа
 *
 * @property int $status статус из OrderStatusEnum
 * @property int $payment_status статус оплаты
 *
 * @property string $customer_email почта покупателя
 */
class OrderPayload extends Payload
{
}
