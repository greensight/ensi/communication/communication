<?php

namespace App\Domain\Kafka\Messages\Listen\Event\Tests;

use Ensi\LaravelTestFactories\Factory;
use RdKafka\Message;

class SendConfirmationCodeEventMessageFactory extends Factory
{
    protected function definition(): array
    {
        return [
            'user_id' => $this->faker->nullable()->modelId(),
            'value' => (string)$this->faker->numberBetween(1000, 9999),
        ];
    }

    public function make(array $extra = []): Message
    {
        $message = new Message();
        $message->payload = json_encode($this->makeArray($extra));

        return $message;
    }
}
