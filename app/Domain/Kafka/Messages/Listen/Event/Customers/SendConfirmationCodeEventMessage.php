<?php

namespace App\Domain\Kafka\Messages\Listen\Event\Customers;

use App\Domain\Kafka\Messages\Listen\Event\EventMessage;
use App\Domain\Kafka\Messages\Listen\Event\Tests\SendConfirmationCodeEventMessageFactory;

/**
 * @property int|null $user_id - идентификатор пользователя
 * @property string $value - код подтверждения
 */
class SendConfirmationCodeEventMessage extends EventMessage
{
    public static function factory(): SendConfirmationCodeEventMessageFactory
    {
        return SendConfirmationCodeEventMessageFactory::new();
    }
}
