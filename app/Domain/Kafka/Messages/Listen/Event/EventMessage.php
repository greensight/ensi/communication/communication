<?php

namespace App\Domain\Kafka\Messages\Listen\Event;

use App\Domain\Kafka\Messages\Listen\ModelEvent\Payload;
use RdKafka\Message;

abstract class EventMessage extends Payload
{
    public static function makeFromRdKafka(Message $message): static
    {
        $payload = json_decode($message->payload, true);

        return new static($payload);
    }
}
