<?php

use App\Domain\Kafka\Actions\Listen\Units\ListenAdminUserAction;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Units\AdminUsers\AdminUserEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Units\AdminUsers\AdminUserPayload;
use App\Domain\Notifications\Mail\AdminUsers\SendUpdatedAdminUserMailAction;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka', 'kafka-listen-admin-user');

test("Action ListenAdminUserAction updated user", function () {
    /** @var IntegrationTestCase $this */
    $payload = AdminUserPayload::factory()->withEmail()->make();
    $message = AdminUserEventMessage::factory()
        ->payload($payload)
        ->event(ModelEventMessage::UPDATE)
        ->make();

    $this->mock(SendUpdatedAdminUserMailAction::class)->shouldReceive('execute')->once();

    resolve(ListenAdminUserAction::class)->execute($message);
});
