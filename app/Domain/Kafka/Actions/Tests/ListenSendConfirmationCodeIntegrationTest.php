<?php

use App\Domain\Kafka\Actions\Listen\Customers\ListenSendConfirmationCodeAction;
use App\Domain\Kafka\Messages\Listen\Event\Customers\SendConfirmationCodeEventMessage;
use App\Domain\Notifications\Actions\CreateNotificationAction;
use App\Domain\Templates\Holders\HoldersManager;
use Illuminate\Testing\Assert;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka');

test("Action ListenSendConfirmationCodeAction create notification", function () {
    /** @var IntegrationTestCase $this */
    $userId = 1;
    $code = 'code';
    $message = SendConfirmationCodeEventMessage::factory()->make(['user_id' => $userId, 'value' => $code]);

    $this->mock(CreateNotificationAction::class)->shouldReceive('execute')->once();

    resolve(ListenSendConfirmationCodeAction::class)->execute($message);

    $holderManager = resolve(HoldersManager::class);
    Assert::assertEquals($code, $holderManager->confirmationCodesHolder->getValue($userId));
});
