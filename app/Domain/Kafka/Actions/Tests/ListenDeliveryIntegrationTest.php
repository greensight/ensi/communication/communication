<?php

use App\Domain\Kafka\Actions\Listen\Orders\ListenDeliveryAction;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\Deliveries\DeliveryEventMessage;
use App\Domain\Notifications\Actions\CreateNotificationAction;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka');

test("Action ListenDeliveryAction create notification", function (string $event) {
    /** @var IntegrationTestCase $this */
    $message = DeliveryEventMessage::factory()
        ->event($event)
        ->dirty(['status'])
        ->make();

    $this->mock(CreateNotificationAction::class)->shouldReceive('execute')->once();

    resolve(ListenDeliveryAction::class)->execute($message);
})->with([ModelEventMessage::CREATE, ModelEventMessage::UPDATE]);
