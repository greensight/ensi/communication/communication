<?php

use App\Domain\Events\Actions\Orders\AdminOrderMailNotify;
use App\Domain\Kafka\Actions\Listen\Orders\ListenOrderAction;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\Orders\OrderEventMessage;
use App\Domain\Notifications\Actions\CreateNotificationAction;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'kafka');

test("Action ListenDeliveryAction create notification", function (string $event) {
    /** @var IntegrationTestCase $this */
    $message = OrderEventMessage::factory()
        ->event($event)
        ->dirty(['status'])
        ->make();

    $this->mock(CreateNotificationAction::class)->shouldReceive('execute')->once();
    $this->mock(AdminOrderMailNotify::class)->shouldReceive('payed', 'cancelled', 'delivered');

    resolve(ListenOrderAction::class)->execute($message);
})->with([ModelEventMessage::CREATE, ModelEventMessage::UPDATE]);
