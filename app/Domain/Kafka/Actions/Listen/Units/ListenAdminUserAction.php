<?php

namespace App\Domain\Kafka\Actions\Listen\Units;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Units\AdminUsers\AdminUserEventMessage;
use App\Domain\Notifications\Mail\AdminUsers\SendUpdatedAdminUserMailAction;
use RdKafka\Message;

class ListenAdminUserAction
{
    public function __construct(protected readonly SendUpdatedAdminUserMailAction $sendUpdatedAdminUserMailAction)
    {
    }

    public function execute(Message $message): void
    {
        $eventMessage = AdminUserEventMessage::makeFromRdKafka($message);

        if (blank($eventMessage->attributes->email)) {
            return;
        }

        if ($eventMessage->event == ModelEventMessage::UPDATE) {
            $this->sendUpdatedAdminUserMailAction->execute($eventMessage->attributes);
        }
    }
}
