<?php

namespace App\Domain\Kafka\Actions\Listen\Orders;

use App\Domain\Events\Actions\Orders\AdminOrderMailNotify;
use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\Orders\OrderEventMessage;
use App\Domain\Notifications\Actions\CreateNotificationAction;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use Ensi\OmsClient\Dto\OrderStatusEnum;
use Ensi\OmsClient\Dto\PaymentStatusEnum;
use Exception;
use RdKafka\Message;

class ListenOrderAction
{
    public function __construct(
        protected readonly CreateNotificationAction $createNotificationAction,
        protected readonly AdminOrderMailNotify $adminOrderMailNotify,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = OrderEventMessage::makeFromRdKafka($message);
        $orderData = $eventMessage->attributes;

        if ($eventMessage->event === ModelEventMessage::CREATE || in_array('status', $eventMessage->dirty)) {
            $event = match ($orderData->status) {
                OrderStatusEnum::WAIT_PAY => NotificationEventEnum::ORDER_WAIT_PAY,
                OrderStatusEnum::NEW => NotificationEventEnum::ORDER_NEW,
                OrderStatusEnum::APPROVED => NotificationEventEnum::ORDER_APPROVED,
                OrderStatusEnum::DONE => NotificationEventEnum::ORDER_DONE,
                OrderStatusEnum::CANCELED => NotificationEventEnum::ORDER_CANCELED,
                default => throw new Exception('Unexpected match value'),
            };

            $this->createNotificationAction->execute($orderData->id, $event);
        }

        if ($eventMessage->event == ModelEventMessage::UPDATE) {
            $this->updated($eventMessage);
        }
    }

    public function updated(OrderEventMessage $order): void
    {
        $this->notifyIfOrderPaid($order);
        $this->notifyIfOrderCancelled($order);
        $this->notifyIfOrderDelivered($order);
    }

    private function notifyIfOrderPaid(OrderEventMessage $order): void
    {
        if (in_array('payment_status', $order->dirty) && $order->attributes->payment_status == PaymentStatusEnum::PAID) {
            $this->adminOrderMailNotify->payed($order->attributes);
        }
    }

    private function notifyIfOrderCancelled(OrderEventMessage $order): void
    {
        if (in_array('status', $order->dirty) && $order->attributes->status == OrderStatusEnum::CANCELED) {
            $this->adminOrderMailNotify->cancelled($order->attributes);
        }
    }

    private function notifyIfOrderDelivered(OrderEventMessage $order): void
    {
        if (in_array('status', $order->dirty) && $order->attributes->status == OrderStatusEnum::DONE) {
            $this->adminOrderMailNotify->delivered($order->attributes);
        }
    }
}
