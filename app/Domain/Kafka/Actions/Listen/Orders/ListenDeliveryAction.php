<?php

namespace App\Domain\Kafka\Actions\Listen\Orders;

use App\Domain\Kafka\Messages\Listen\ModelEvent\ModelEventMessage;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Orders\Deliveries\DeliveryEventMessage;
use App\Domain\Notifications\Actions\CreateNotificationAction;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use Ensi\OmsClient\Dto\DeliveryStatusEnum;
use Exception;
use RdKafka\Message;

class ListenDeliveryAction
{
    public function __construct(protected readonly CreateNotificationAction $createNotificationAction)
    {
    }

    public function execute(Message $message): void
    {
        $eventMessage = DeliveryEventMessage::makeFromRdKafka($message);
        $deliveryData = $eventMessage->attributes;

        if ($eventMessage->event === ModelEventMessage::CREATE || in_array('status', $eventMessage->dirty)) {
            $event = match ($deliveryData->status) {
                DeliveryStatusEnum::NEW => NotificationEventEnum::DELIVERY_NEW,
                DeliveryStatusEnum::ASSEMBLED => NotificationEventEnum::DELIVERY_ASSEMBLED,
                DeliveryStatusEnum::TRANSFER => NotificationEventEnum::DELIVERY_TRANSFER,
                DeliveryStatusEnum::READY_TO_PICKUP => NotificationEventEnum::DELIVERY_READY_TO_PICKUP,
                DeliveryStatusEnum::DONE => NotificationEventEnum::DELIVERY_DONE,
                DeliveryStatusEnum::CANCELED => NotificationEventEnum::DELIVERY_CANCELED,
                default => throw new Exception('Unexpected match value'),
            };

            $this->createNotificationAction->execute($deliveryData->id, $event);
        }
    }
}
