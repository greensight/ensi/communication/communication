<?php

namespace App\Domain\Kafka\Actions\Listen\Customers;

use App\Domain\Kafka\Messages\Listen\Event\Customers\SendConfirmationCodeEventMessage;
use App\Domain\Notifications\Actions\CreateNotificationAction;
use App\Domain\Templates\Holders\HoldersManager;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use RdKafka\Message;

class ListenSendConfirmationCodeAction
{
    public function __construct(
        protected readonly CreateNotificationAction $createNotificationAction,
        protected readonly HoldersManager $holdersManager,
    ) {
    }

    public function execute(Message $message): void
    {
        $eventMessage = SendConfirmationCodeEventMessage::makeFromRdKafka($message);

        if ($eventMessage->user_id) {
            $this->holdersManager->confirmationCodesHolder->setValue($eventMessage->user_id, $eventMessage->value);
            $this->createNotificationAction->execute($eventMessage->user_id, NotificationEventEnum::SEND_CONFIRMATION_CODE);
        }
    }
}
