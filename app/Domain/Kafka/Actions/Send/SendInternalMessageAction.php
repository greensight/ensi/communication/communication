<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Kafka\Messages\Send\Messages\InternalMessage;
use App\Domain\Kafka\Messages\Send\Messages\InternalMessagePayload;

class SendInternalMessageAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(array $message): void
    {
        $modelEvent = new InternalMessage(InternalMessagePayload::make($message));
        $this->sendAction->execute($modelEvent);
    }
}
