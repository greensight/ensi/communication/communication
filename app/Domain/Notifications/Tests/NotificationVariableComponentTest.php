<?php

namespace App\Domain\Notifications\Tests;

use App\Domain\Notifications\Enums\NotificationEvent;
use App\Domain\Notifications\Enums\NotificationVariable;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use Illuminate\Testing\Assert;
use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'notifications');

function checkItemsExistence(array $enums): void
{
    foreach ($enums as $enum) {
        if ($enum->isList()) {
            Assert::assertNotNull($enum->items);
            checkItemsExistence($enum->items);
        } else {
            Assert::assertNull($enum->items);
        }
    }
}

test('Enum NotificationVariableEnum all leaf variables are returned sublist of tags', function (array $enums) {
    checkItemsExistence($enums);
})->with([
    'order' => [NotificationVariable::order()],
    'delivery' => [NotificationVariable::delivery()],
    'authorisation' => [NotificationVariable::authorisation()],
]);

test('NotificationEventEnum has the same elements as NotificationEvent', function ($value) {
    Assert::assertTrue(in_array($value, NotificationEvent::all()));
})->with(NotificationEventEnum::cases());
