<?php

namespace App\Domain\Notifications\Tests;

use App\Domain\Common\Tests\Factories\Customers\CustomerFactory;
use App\Domain\Common\Tests\Factories\Orders\DeliveryFactory;
use App\Domain\Common\Tests\Factories\Orders\OrderFactory;
use App\Domain\Notifications\Actions\CreateNotificationAction;
use App\Domain\Notifications\Enums\NotificationEvent;
use App\Domain\Notifications\Models\Notification;
use App\Domain\Notifications\Models\NotificationSetting;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;

use function Pest\Laravel\assertDatabaseHas;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class);
uses()->group('component', 'notifications');

test('Action CreateNotificationAction successfully', function (NotificationEventEnum $event) {
    /** @var CreateNotificationAction $action */

    $mainEntityId = 1;
    $customerId = 1;

    /** @var NotificationSetting $setting */
    $setting = NotificationSetting::factory()->withEvent($event)->create();

    if (in_array($event, NotificationEvent::order())) {
        $this->mockOmsOrdersApi()
            ->shouldReceive('searchOrders')
            ->andReturn(
                OrderFactory::new()->makeResponseSearch([
                    ['id' => $mainEntityId, 'customer_id' => $customerId],
                ])
            );
    }

    if (in_array($event, NotificationEvent::authorisation())) {
        $this->mockCustomersCustomersApi()
            ->shouldReceive('searchCustomers')
            ->andReturn(
                CustomerFactory::new()->makeResponseSearch([
                    ['user_id' => $mainEntityId, 'id' => $customerId],
                ])
            );
    }

    if (in_array($event, NotificationEvent::delivery())) {
        $linkId = 1;
        $this->mockOmsDeliveriesApi()
            ->shouldReceive('searchDeliveries')
            ->andReturn(
                DeliveryFactory::new()->makeResponseSearch([
                    ['id' => $mainEntityId, 'order_id' => $linkId],
                ])
            );

        $this->mockOmsOrdersApi()
            ->shouldReceive('searchOrders')
            ->andReturn(
                OrderFactory::new()->makeResponseSearch([
                    ['id' => $linkId, 'customer_id' => $customerId],
                ])
            );
    }

    $action = resolve(CreateNotificationAction::class);
    $action->execute($mainEntityId, $event);

    assertDatabaseHas(Notification::class, [
        'event' => $event, 'customer_id' => $customerId, 'text' => $setting->text,
    ]);
})->with([
    'order' => current(NotificationEvent::order()),
    'delivery' => current(NotificationEvent::delivery()),
    'authorisation' => current(NotificationEvent::authorisation()),
]);
