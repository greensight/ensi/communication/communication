<?php

namespace App\Domain\Notifications\Models;

use App\Domain\Notifications\Models\Tests\Factories\NotificationFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationChannelEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Casts\AsEnumArrayObject;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id Notification ID
 * @property int $customer_id Customer ID
 * @property NotificationEventEnum $event ID of notification event
 * @property NotificationChannelEnum[] $channels ID of sending channels
 * @property string|null $theme Notification theme
 * @property string $text Notification text
 * @property bool $is_viewed Has the notification been viewed (Available for the Showcase channel)
 *
 * @property CarbonInterface $created_at Date of creation
 * @property CarbonInterface $updated_at Date of update
 */
class Notification extends Model
{
    protected $table = 'notifications';

    protected $attributes = [
        'is_viewed' => false,
    ];

    protected $casts = [
        'event' => NotificationEventEnum::class,
        'channels' => AsEnumArrayObject::class . ':' . NotificationChannelEnum::class,
    ];

    protected $fillable = ['is_viewed'];

    public static function factory(): NotificationFactory
    {
        return NotificationFactory::new();
    }
}
