<?php

namespace App\Domain\Notifications\Models;

use App\Domain\Notifications\Models\Tests\Factories\NotificationSettingFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationChannelEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Casts\AsEnumArrayObject;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id ID of the notification settings
 * @property string $name Notification name
 * @property NotificationEventEnum $event Event notification ID from NotificationEventEnum
 * @property NotificationChannelEnum[] $channels Sending channel from NotificationChannelEnum
 * @property string|null $theme Notification theme
 * @property string $text Notification text
 *
 * @property CarbonInterface $created_at Date of creation
 * @property CarbonInterface $updated_at Date of update
 */
class NotificationSetting extends Model
{
    protected $table = 'notification_settings';

    protected $casts = [
        'event' => NotificationEventEnum::class,
        'channels' => AsEnumArrayObject::class . ':' . NotificationChannelEnum::class,
    ];

    protected $fillable = ['name', 'event', 'channels', 'theme', 'text'];

    /** @noinspection PhpIncompatibleReturnTypeInspection */
    public static function findByEvent(NotificationEventEnum $event): ?self
    {
        return self::query()->where('event', $event)->first();
    }

    public static function factory(): NotificationSettingFactory
    {
        return NotificationSettingFactory::new();
    }
}
