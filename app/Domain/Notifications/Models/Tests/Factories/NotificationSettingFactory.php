<?php

namespace App\Domain\Notifications\Models\Tests\Factories;

use App\Domain\Notifications\Models\NotificationSetting;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationChannelEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class NotificationSettingFactory extends BaseModelFactory
{
    protected $model = NotificationSetting::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->sentence(3),
            'event' => $this->faker->unique()->randomEnum(NotificationEventEnum::cases()),
            'channels' => $this->faker->randomList(fn () => $this->faker->randomEnum(NotificationChannelEnum::cases()), min: 1),
            'theme' => $this->faker->nullable()->sentence(3),
            'text' => $this->faker->text(),
        ];
    }

    public function withEvent(NotificationEventEnum $event): static
    {
        return $this->state(['event' => $event]);
    }
}
