<?php

namespace App\Domain\Notifications\Models\Tests\Factories;

use App\Domain\Notifications\Models\Notification;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationChannelEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use Ensi\LaravelTestFactories\BaseModelFactory;

class NotificationFactory extends BaseModelFactory
{
    protected $model = Notification::class;

    public function definition(): array
    {
        return [
            'customer_id' => $this->faker->modelId(),
            'event' => $this->faker->randomEnum(NotificationEventEnum::cases()),
            'channels' => $this->faker->randomList(fn () => $this->faker->randomEnum(NotificationChannelEnum::cases()), min: 1),
            'theme' => $this->faker->nullable()->sentence(3),
            'text' => $this->faker->text(),
            'is_viewed' => $this->faker->boolean(),
        ];
    }
}
