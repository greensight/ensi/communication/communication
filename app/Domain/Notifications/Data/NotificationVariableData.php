<?php

namespace App\Domain\Notifications\Data;

use App\Domain\Notifications\Enums\NotificationEvent;
use App\Domain\Notifications\Enums\NotificationVariable;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum as EventEnum;

class NotificationVariableData
{
    /** @param NotificationVariable[] $variables */
    public function __construct(public EventEnum $event, public array $variables)
    {
    }

    public static function availableVariables(EventEnum $event): array
    {
        return match (true) {
            in_array($event, NotificationEvent::order()) => NotificationVariable::order(),
            in_array($event, NotificationEvent::delivery()) => NotificationVariable::delivery(),
            in_array($event, NotificationEvent::authorisation()) => NotificationVariable::authorisation(),
            default => [],
        };
    }

    /** @return self[] */
    public static function all(): array
    {
        $all = [];
        foreach (NotificationEvent::all() as $event) {
            $all[] = new self($event, self::availableVariables($event));
        }

        return $all;
    }
}
