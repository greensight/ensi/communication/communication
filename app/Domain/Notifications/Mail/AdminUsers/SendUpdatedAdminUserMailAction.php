<?php

namespace App\Domain\Notifications\Mail\AdminUsers;

use App\Domain\Events\Mail\User\UserUpdated;
use App\Domain\Kafka\Messages\Listen\ModelEvent\Units\AdminUsers\AdminUserPayload;
use Illuminate\Support\Facades\Mail;

class SendUpdatedAdminUserMailAction
{
    public function execute(AdminUserPayload $user): void
    {
        Mail::to($user->email)->send(new UserUpdated([
            'full_name' => $user->full_name,
            'subject' => __('messages.users.user_updated.subject'),
        ]));
    }
}
