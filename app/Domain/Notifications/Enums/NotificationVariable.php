<?php

namespace App\Domain\Notifications\Enums;

use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationVariableEnum;

class NotificationVariable
{
    public function __construct(public NotificationVariableEnum $data, public ?array $items = null)
    {
    }

    /** @return self[] */
    public static function order(): array
    {
        return [
            new self(NotificationVariableEnum::ORDER_PRODUCTS, self::productData()),
            new self(NotificationVariableEnum::ORDER_RECEIVER_NAME),
            new self(NotificationVariableEnum::ORDER_RECEIVER_PHONE),
            new self(NotificationVariableEnum::ORDER_NUMBER),
            new self(NotificationVariableEnum::ORDER_PAYMENT_LINK),
            new self(NotificationVariableEnum::ORDER_PRICE),
            new self(NotificationVariableEnum::ORDER_PRODUCTS_PRICE),
            new self(NotificationVariableEnum::ORDER_DELIVERY_PRICE),
            new self(NotificationVariableEnum::ORDER_DELIVERY_METHOD),
            new self(NotificationVariableEnum::ORDER_DELIVERY_ADDRESS),
            new self(NotificationVariableEnum::ORDER_DELIVERY_POINT),
            new self(NotificationVariableEnum::ORDER_DELIVERY_COMMENT),
            new self(NotificationVariableEnum::ORDER_DELIVERIES, self::deliveryData()),
            new self(NotificationVariableEnum::CUSTOMER_NAME),
        ];
    }

    /** @return self[] */
    public static function delivery(): array
    {
        return [
            new self(NotificationVariableEnum::DELIVERY_PRODUCTS, self::productData()),
            new self(NotificationVariableEnum::ORDER_RECEIVER_NAME),
            new self(NotificationVariableEnum::ORDER_RECEIVER_PHONE),
            new self(NotificationVariableEnum::ORDER_NUMBER),
            new self(NotificationVariableEnum::ORDER_PAYMENT_LINK),
            new self(NotificationVariableEnum::ORDER_PRICE),
            new self(NotificationVariableEnum::ORDER_PRODUCTS_PRICE),
            new self(NotificationVariableEnum::ORDER_DELIVERY_PRICE),
            new self(NotificationVariableEnum::ORDER_DELIVERY_METHOD),
            new self(NotificationVariableEnum::ORDER_DELIVERY_ADDRESS),
            new self(NotificationVariableEnum::ORDER_DELIVERY_POINT),
            new self(NotificationVariableEnum::ORDER_DELIVERY_COMMENT),
            new self(NotificationVariableEnum::ORDER_DELIVERY_NUMBER),
            new self(NotificationVariableEnum::ORDER_DELIVERY_DATE),
            new self(NotificationVariableEnum::ORDER_DELIVERY_TIMESLOT),
            new self(NotificationVariableEnum::CUSTOMER_NAME),
        ];
    }

    /** @return self[] */
    public static function authorisation(): array
    {
        return [
            new self(NotificationVariableEnum::CUSTOMER_NAME),
            new self(NotificationVariableEnum::CONFIRMATION_CODE),
        ];
    }

    public function isList(): bool
    {
        return in_array($this->data, self::listVariable());
    }

    public static function name(NotificationVariableEnum $enum): string
    {
        return match ($enum) {
            NotificationVariableEnum::ORDER_PRODUCTS => 'Cписок продуктов в заказе',
            NotificationVariableEnum::DELIVERY_PRODUCTS => 'Cписок продуктов в отправление',
            NotificationVariableEnum::PRODUCT_NAME => 'Название товара',
            NotificationVariableEnum::PRODUCT_QTY => 'Количества товара в позиции',
            NotificationVariableEnum::PRODUCT_PRICE => 'Цена позиции с товаром со скидкой',
            NotificationVariableEnum::PRODUCT_COST => 'Цена позиции с товаром без скидки',
            NotificationVariableEnum::PRODUCT_IMAGE => 'Ссылка главного изображения товара',
            NotificationVariableEnum::ORDER_RECEIVER_NAME => 'Имя получателя',
            NotificationVariableEnum::ORDER_RECEIVER_PHONE => 'Телефон получателя',
            NotificationVariableEnum::ORDER_NUMBER => 'Номер заказа',
            NotificationVariableEnum::ORDER_PAYMENT_LINK => 'Ссылка на оплату',
            NotificationVariableEnum::ORDER_PRICE => 'Итогова стоимость заказа',
            NotificationVariableEnum::ORDER_PRODUCTS_PRICE => 'Стоимость товаров в заказе',
            NotificationVariableEnum::ORDER_DELIVERY_PRICE => 'Стоимость доставки',
            NotificationVariableEnum::ORDER_DELIVERY_METHOD => 'Метод доставки',
            NotificationVariableEnum::ORDER_DELIVERY_ADDRESS => 'Адрес доставки',
            NotificationVariableEnum::ORDER_DELIVERY_POINT => 'Точка самовывоза',
            NotificationVariableEnum::ORDER_DELIVERY_COMMENT => 'Комментарий к доставке',
            NotificationVariableEnum::ORDER_DELIVERIES => 'Отправления в заказе',
            NotificationVariableEnum::ORDER_DELIVERY_NUMBER => 'Номер отправления',
            NotificationVariableEnum::ORDER_DELIVERY_DATE => 'Дата доставки отправления',
            NotificationVariableEnum::ORDER_DELIVERY_TIMESLOT => 'Временной диапазон доставки',
            NotificationVariableEnum::CUSTOMER_NAME => 'Имя клиента',
            NotificationVariableEnum::CONFIRMATION_CODE => 'Код подтверждения',
        };
    }

    /** @return self[] */
    protected static function deliveryData(): array
    {
        return [
            new self(NotificationVariableEnum::ORDER_DELIVERY_NUMBER),
            new self(NotificationVariableEnum::ORDER_DELIVERY_DATE),
            new self(NotificationVariableEnum::ORDER_DELIVERY_TIMESLOT),
            new self(NotificationVariableEnum::DELIVERY_PRODUCTS, self::productData()),
        ];
    }

    /** @return self[] */
    protected static function productData(): array
    {
        return [
            new self(NotificationVariableEnum::PRODUCT_NAME),
            new self(NotificationVariableEnum::PRODUCT_QTY),
            new self(NotificationVariableEnum::PRODUCT_PRICE),
            new self(NotificationVariableEnum::PRODUCT_COST),
            new self(NotificationVariableEnum::PRODUCT_IMAGE),
        ];
    }

    /** @return NotificationVariableEnum[] */
    protected static function listVariable(): array
    {
        return [
            NotificationVariableEnum::ORDER_PRODUCTS,
            NotificationVariableEnum::DELIVERY_PRODUCTS,
            NotificationVariableEnum::ORDER_DELIVERIES,
        ];
    }
}
