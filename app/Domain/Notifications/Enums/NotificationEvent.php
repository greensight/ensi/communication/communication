<?php

namespace App\Domain\Notifications\Enums;

use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;

class NotificationEvent
{
    /** @return NotificationEventEnum[] */
    public static function order(): array
    {
        return [
            NotificationEventEnum::ORDER_WAIT_PAY,
            NotificationEventEnum::ORDER_NEW,
            NotificationEventEnum::ORDER_APPROVED,
            NotificationEventEnum::ORDER_DONE,
            NotificationEventEnum::ORDER_CANCELED,
        ];
    }

    public static function delivery(): array
    {
        return [
            NotificationEventEnum::DELIVERY_NEW,
            NotificationEventEnum::DELIVERY_ASSEMBLED,
            NotificationEventEnum::DELIVERY_TRANSFER,
            NotificationEventEnum::DELIVERY_READY_TO_PICKUP,
            NotificationEventEnum::DELIVERY_DONE,
            NotificationEventEnum::DELIVERY_CANCELED,
        ];
    }

    /** @return NotificationEventEnum[] */
    public static function authorisation(): array
    {
        return [
            NotificationEventEnum::SEND_CONFIRMATION_CODE,
        ];
    }

    /** @return NotificationEventEnum[] */
    public static function all(): array
    {
        return array_merge(self::order(), self::delivery(), self::authorisation());
    }
}
