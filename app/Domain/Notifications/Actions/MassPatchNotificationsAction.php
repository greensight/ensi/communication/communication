<?php

namespace App\Domain\Notifications\Actions;

use App\Domain\Notifications\Models\Notification;
use Illuminate\Database\Eloquent\Builder as EloquentBuilder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\LazyCollection;
use Webmozart\Assert\Assert;

class MassPatchNotificationsAction
{
    public function __construct(protected readonly PatchNotificationAction $patchNotificationAction)
    {
    }

    public function execute(EloquentBuilder $query, array $fields): void
    {
        Assert::isInstanceOf($query->getModel(), Notification::class, 'Notification query expected');

        /** @var LazyCollection<int, Notification> $notifications */
        $notifications = $query->select('id')->lazyById();

        DB::transaction(function () use ($notifications, $fields) {
            $notifications->each(
                fn (Notification $notification, int $index) => $this->patchNotificationAction->execute($notification->id, $fields)
            );
        });
    }
}
