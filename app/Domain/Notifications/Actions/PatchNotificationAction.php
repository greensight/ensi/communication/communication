<?php

namespace App\Domain\Notifications\Actions;

use App\Domain\Notifications\Models\Notification;

class PatchNotificationAction
{
    public function execute(int $id, array $fields): Notification
    {
        /** @var Notification $notification */
        $notification = Notification::query()->findOrFail($id);
        $notification->update($fields);

        return $notification;
    }
}
