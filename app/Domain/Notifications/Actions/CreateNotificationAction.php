<?php

namespace App\Domain\Notifications\Actions;

use App\Domain\Notifications\Enums\NotificationEvent;
use App\Domain\Notifications\Models\Notification;
use App\Domain\Notifications\Models\NotificationSetting;
use App\Domain\Templates\Holders\HoldersManager;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use Exception;

class CreateNotificationAction
{
    public function __construct(
        protected readonly FillNotificationTemplateAction $fillTemplateAction,
        protected readonly HoldersManager $holdersManager,
    ) {
    }

    public function execute(int $mainEntityId, NotificationEventEnum $event): void
    {
        $setting = NotificationSetting::findByEvent($event);

        if ($setting === null) {
            return;
        }

        $notification = new Notification();
        $notification->event = $setting->event;
        $notification->channels = $setting->channels;
        $notification->theme = $this->enrichTemplate($mainEntityId, $setting->event, $setting->theme);
        $notification->text = $this->enrichTemplate($mainEntityId, $setting->event, $setting->text);
        $notification->customer_id = $this->getCustomerId($mainEntityId, $event);

        $notification->save();
    }

    protected function getCustomerId(int $mainEntityId, NotificationEventEnum $event): int
    {
        return match (true) {
            in_array($event, NotificationEvent::order()) => $this->holdersManager->ordersHolder->get($mainEntityId)->getCustomerId(),
            in_array($event, NotificationEvent::authorisation()) => $this->holdersManager->userCustomersHolder->get($mainEntityId)->getId(),
            in_array($event, NotificationEvent::delivery()) => $this->holdersManager->ordersHolder
                ->get(
                    $this->holdersManager->deliveriesHolder->get($mainEntityId)->getOrderId()
                )->getCustomerId(),
            default => throw new Exception("Unexpected match value for this event {$event->value}"),
        };

    }

    protected function enrichTemplate(int $mainEntityId, NotificationEventEnum $event, ?string $template): ?string
    {
        if ($template === null) {
            return null;
        }

        return $this->fillTemplateAction->execute($mainEntityId, $event, $template);
    }
}
