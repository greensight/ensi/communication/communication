<?php

namespace App\Domain\Notifications\Actions;

use App\Domain\Notifications\Models\NotificationSetting;

class PatchNotificationSettingAction
{
    public function execute(int $id, array $fields): NotificationSetting
    {
        /** @var NotificationSetting $notificationSetting */
        $notificationSetting = NotificationSetting::query()->findOrFail($id);
        $notificationSetting->update($fields);

        return $notificationSetting;
    }
}
