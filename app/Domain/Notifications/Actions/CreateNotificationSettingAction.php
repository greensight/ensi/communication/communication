<?php

namespace App\Domain\Notifications\Actions;

use App\Domain\Notifications\Models\NotificationSetting;

class CreateNotificationSettingAction
{
    public function execute(array $fields): NotificationSetting
    {
        $notificationSetting = new NotificationSetting();
        $notificationSetting->fill($fields);
        $notificationSetting->save();

        return $notificationSetting;
    }
}
