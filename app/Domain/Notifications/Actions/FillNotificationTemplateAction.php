<?php

namespace App\Domain\Notifications\Actions;

use App\Domain\Notifications\Data\NotificationVariableData;
use App\Domain\Notifications\Enums\NotificationEvent;
use App\Domain\Templates\Actions\Data\Variable;
use App\Domain\Templates\Actions\ReplaceVariablesAction;
use App\Domain\Templates\Contracts\ValueDictionary;
use App\Domain\Templates\Data\AuthorisationValueData;
use App\Domain\Templates\Data\DeliveryValueData;
use App\Domain\Templates\Data\OrderValueData;
use App\Domain\Templates\Holders\HoldersManager;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use Exception;

class FillNotificationTemplateAction
{
    public function __construct(
        protected readonly ReplaceVariablesAction $replaceVariablesAction,
        protected readonly HoldersManager $holdersManager,
    ) {
    }

    public function execute(int $mainEntityId, NotificationEventEnum $event, string $template): string
    {
        $valueDictionary = $this->getValueDictionary($mainEntityId, $event);
        $variables = $this->getVariables($event);

        return $this->replaceVariablesAction->execute($template, $variables, $valueDictionary);

    }

    protected function getVariables(NotificationEventEnum $event): array
    {
        $notificationVariables = NotificationVariableData::availableVariables($event);

        return Variable::mapFromNotificationVariable($notificationVariables);
    }

    protected function getValueDictionary(int $mainEntityId, NotificationEventEnum $event): ValueDictionary
    {
        return match (true) {
            in_array($event, NotificationEvent::order()) => new OrderValueData($mainEntityId, $this->holdersManager),
            in_array($event, NotificationEvent::delivery()) => new DeliveryValueData($mainEntityId, $this->holdersManager),
            in_array($event, NotificationEvent::authorisation()) => new AuthorisationValueData($mainEntityId, $this->holdersManager),
            default => throw new Exception('Unexpected match value'),
        };
    }
}
