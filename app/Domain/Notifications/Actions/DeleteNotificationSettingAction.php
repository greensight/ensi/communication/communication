<?php

namespace App\Domain\Notifications\Actions;

use App\Domain\Notifications\Models\NotificationSetting;

class DeleteNotificationSettingAction
{
    public function execute(int $id): void
    {
        NotificationSetting::destroy($id);
    }
}
