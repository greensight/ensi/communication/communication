<?php

namespace App\Http\ApiV1\Support\Resources;

class DataResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return $this->resource;
    }
}
