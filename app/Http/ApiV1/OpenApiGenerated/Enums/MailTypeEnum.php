<?php

/**
 * NOTE: This file is auto generated by OpenAPI Generator.
 * Do NOT edit it manually. Run `php artisan openapi:generate-server`.
 */

namespace App\Http\ApiV1\OpenApiGenerated\Enums;

/**
 * Тип письма. Расшифровка значений:
 * * `order-paid` - Письмо покупателю об оплаченном заказе
 * * `order-cancelled` - Письмо покупателю об отмененном заказе
 * * `order-delivered` - Письмо покупателю о доставленном заказе
 * * `order-returned` - Письмо покупателю о недоставленном заказе
 * * `manager-order-paid` - Письмо сотруднику об оплаченном заказе
 * * `manager-order-cancelled` - Письмо сотруднику об отмененном заказе
 * * `manager-order-delivered` - Письмо сотруднику о доставленном заказе
 * * `manager-order-returned` - Письмо сотруднику о недоставленном заказе
 */
enum MailTypeEnum: string
{
    /** Письмо покупателю об оплаченном заказе */
    case ORDER_PAID = 'order-paid';
    /** Письмо покупателю об отмененном заказе */
    case ORDER_CANCELLED = 'order-cancelled';
    /** Письмо покупателю о доставленном заказе */
    case ORDER_DELIVERED = 'order-delivered';
    /** Письмо покупателю о недоставленном заказе */
    case ORDER_RETURNED = 'order-returned';
    /** Письмо сотруднику об оплаченном заказе */
    case MANAGER_ORDER_PAID = 'manager-order-paid';
    /** Письмо сотруднику об отмененном заказе */
    case MANAGER_ORDER_CANCELED = 'manager-order-cancelled';
    /** Письмо сотруднику о доставленном заказе */
    case MANAGER_ORDER_DELIVERED = 'manager-order-delivered';
    /** Письмо сотруднику о недоставленном заказе */
    case MANAGER_ORDER_RETURNED = 'manager-order-returned';
}
