<?php

namespace App\Http\ApiV1\Modules\Control\Queries;

use App\Domain\Control\Models\Status;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StatusesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Status::query());

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('active'),
        ]);

        $this->defaultSort('id');
    }
}
