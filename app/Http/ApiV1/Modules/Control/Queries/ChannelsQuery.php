<?php

namespace App\Http\ApiV1\Modules\Control\Queries;

use App\Domain\Control\Models\Channel;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class ChannelsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Channel::query());

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
        ]);

        $this->defaultSort('id');
    }
}
