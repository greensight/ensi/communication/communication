<?php

namespace App\Http\ApiV1\Modules\Control\Queries;

use App\Domain\Control\Models\Type;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class TypesQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Type::query());

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('name'),
            AllowedFilter::exact('active'),
        ]);

        $this->defaultSort('id');
    }
}
