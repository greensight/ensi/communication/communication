<?php

namespace App\Http\ApiV1\Modules\Control\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateStatusRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'active' => ['nullable', 'boolean'],
            'default' => ['nullable', 'boolean'],
            'channel' => ['nullable', 'array'],
            'channel.*' => ['integer'],
        ];
    }
}
