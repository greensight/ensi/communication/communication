<?php

namespace App\Http\ApiV1\Modules\Control\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchStatusRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'string'],
            'active' => ['nullable', 'boolean'],
            'default' => ['nullable', 'boolean'],
            'channel' => ['nullable', 'array'],
            'channel.*' => ['integer'],
        ];
    }
}
