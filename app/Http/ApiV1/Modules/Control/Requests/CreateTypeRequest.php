<?php

namespace App\Http\ApiV1\Modules\Control\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class CreateTypeRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'active' => ['nullable', 'boolean'],
            'channel' => ['nullable', 'array'],
            'channel.*' => ['integer'],
        ];
    }
}
