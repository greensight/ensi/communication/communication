<?php

namespace App\Http\ApiV1\Modules\Control\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class ParseEventRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'type' => ['required', 'string'],
            'data' => ['required', 'array'],
        ];
    }
}
