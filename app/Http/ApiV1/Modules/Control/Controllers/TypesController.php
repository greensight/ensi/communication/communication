<?php

namespace App\Http\ApiV1\Modules\Control\Controllers;

use App\Domain\Control\Actions\CreateTypeAction;
use App\Domain\Control\Actions\DeleteTypeAction;
use App\Domain\Control\Actions\PatchTypeAction;
use App\Http\ApiV1\Modules\Control\Queries\TypesQuery;
use App\Http\ApiV1\Modules\Control\Requests\CreateTypeRequest;
use App\Http\ApiV1\Modules\Control\Requests\PatchTypeRequest;
use App\Http\ApiV1\Modules\Control\Resources\TypesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class TypesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, TypesQuery $query): Responsable
    {
        return TypesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function create(CreateTypeRequest $request, CreateTypeAction $action): Responsable
    {
        return new TypesResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchTypeRequest $request, PatchTypeAction $action): Responsable
    {
        return new TypesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteTypeAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
