<?php

namespace App\Http\ApiV1\Modules\Control\Controllers;

use App\Domain\Control\Actions\CreateChannelAction;
use App\Domain\Control\Actions\DeleteChannelAction;
use App\Domain\Control\Actions\PatchChannelAction;
use App\Http\ApiV1\Modules\Control\Queries\ChannelsQuery;
use App\Http\ApiV1\Modules\Control\Requests\CreateChannelRequest;
use App\Http\ApiV1\Modules\Control\Requests\PatchChannelRequest;
use App\Http\ApiV1\Modules\Control\Resources\ChannelsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class ChannelsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ChannelsQuery $query): Responsable
    {
        return ChannelsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function create(CreateChannelRequest $request, CreateChannelAction $action): Responsable
    {
        return new ChannelsResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchChannelRequest $request, PatchChannelAction $action): Responsable
    {
        return new ChannelsResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteChannelAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
