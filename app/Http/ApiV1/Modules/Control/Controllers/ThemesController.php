<?php

namespace App\Http\ApiV1\Modules\Control\Controllers;

use App\Domain\Control\Actions\CreateThemeAction;
use App\Domain\Control\Actions\DeleteThemeAction;
use App\Domain\Control\Actions\PatchThemeAction;
use App\Http\ApiV1\Modules\Control\Queries\ThemesQuery;
use App\Http\ApiV1\Modules\Control\Requests\CreateThemeRequest;
use App\Http\ApiV1\Modules\Control\Requests\PatchThemeRequest;
use App\Http\ApiV1\Modules\Control\Resources\ThemesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class ThemesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, ThemesQuery $query): Responsable
    {
        return ThemesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function create(CreateThemeRequest $request, CreateThemeAction $action): Responsable
    {
        return new ThemesResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchThemeRequest $request, PatchThemeAction $action): Responsable
    {
        return new ThemesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteThemeAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
