<?php

namespace App\Http\ApiV1\Modules\Control\Controllers;

use App\Domain\Events\Actions\ParseEventAction;
use App\Http\ApiV1\Modules\Control\Requests\ParseEventRequest;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class EventsController
{
    public function parse(ParseEventRequest $request, ParseEventAction $action): Responsable
    {
        $action->execute($request);

        return new EmptyResource();
    }
}
