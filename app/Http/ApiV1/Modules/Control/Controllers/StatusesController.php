<?php

namespace App\Http\ApiV1\Modules\Control\Controllers;

use App\Domain\Control\Actions\CreateStatusAction;
use App\Domain\Control\Actions\DeleteStatusAction;
use App\Domain\Control\Actions\PatchStatusAction;
use App\Http\ApiV1\Modules\Control\Queries\StatusesQuery;
use App\Http\ApiV1\Modules\Control\Requests\CreateStatusRequest;
use App\Http\ApiV1\Modules\Control\Requests\PatchStatusRequest;
use App\Http\ApiV1\Modules\Control\Resources\StatusesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class StatusesController
{
    public function search(PageBuilderFactory $pageBuilderFactory, StatusesQuery $query): Responsable
    {
        return StatusesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function create(CreateStatusRequest $request, CreateStatusAction $action): Responsable
    {
        return new StatusesResource($action->execute($request->validated()));
    }

    public function patch(int $id, PatchStatusRequest $request, PatchStatusAction $action): Responsable
    {
        return new StatusesResource($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteStatusAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }
}
