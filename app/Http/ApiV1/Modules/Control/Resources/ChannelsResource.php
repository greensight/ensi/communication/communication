<?php

namespace App\Http\ApiV1\Modules\Control\Resources;

use App\Domain\Control\Models\Channel;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Channel */
class ChannelsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
