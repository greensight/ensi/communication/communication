<?php

namespace App\Http\ApiV1\Modules\Control\Resources;

use App\Domain\Control\Models\Type;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Type */
class TypesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'active' => $this->active,
            'channel' => $this->channel,
        ];
    }
}
