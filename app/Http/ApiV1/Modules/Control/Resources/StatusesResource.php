<?php

namespace App\Http\ApiV1\Modules\Control\Resources;

use App\Domain\Control\Models\Status;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/** @mixin Status */
class StatusesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'active' => $this->active,
            'default' => $this->default,
            'channel' => $this->channel,
        ];
    }
}
