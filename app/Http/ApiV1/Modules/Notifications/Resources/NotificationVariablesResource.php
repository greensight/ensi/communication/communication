<?php

namespace App\Http\ApiV1\Modules\Notifications\Resources;

use App\Domain\Notifications\Enums\NotificationVariable;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin NotificationVariable
 */
class NotificationVariablesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->data,
            'title' => NotificationVariable::name($this->data),
            'items' => self::collection($this->items ?? []),
        ];
    }
}
