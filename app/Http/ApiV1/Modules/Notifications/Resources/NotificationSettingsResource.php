<?php

namespace App\Http\ApiV1\Modules\Notifications\Resources;

use App\Domain\Notifications\Models\NotificationSetting;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin NotificationSetting
 */
class NotificationSettingsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'event' => $this->event,
            'channels' => $this->channels,
            'theme' => $this->theme,
            'text' => $this->text,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
