<?php

namespace App\Http\ApiV1\Modules\Notifications\Resources;

use App\Domain\Notifications\Models\Notification;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Notification
 */
class NotificationsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'customer_id' => $this->customer_id,
            'event' => $this->event,
            'channels' => $this->channels,
            'theme' => $this->theme,
            'text' => $this->text,
            'is_viewed' => $this->is_viewed,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
