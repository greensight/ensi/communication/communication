<?php

namespace App\Http\ApiV1\Modules\Notifications\Resources;

use App\Domain\Notifications\Data\NotificationVariableData;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin NotificationVariableData
 */
class NotificationEventsResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'event' => $this->event,
            'variables' => NotificationVariablesResource::collection($this->variables),
        ];
    }
}
