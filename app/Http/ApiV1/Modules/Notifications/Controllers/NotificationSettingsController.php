<?php

namespace App\Http\ApiV1\Modules\Notifications\Controllers;

use App\Domain\Notifications\Actions\CreateNotificationSettingAction;
use App\Domain\Notifications\Actions\DeleteNotificationSettingAction;
use App\Domain\Notifications\Actions\PatchNotificationSettingAction;
use App\Domain\Notifications\Data\NotificationVariableData;
use App\Http\ApiV1\Modules\Notifications\Queries\NotificationSettingsQuery;
use App\Http\ApiV1\Modules\Notifications\Requests\CreateNotificationSettingRequest;
use App\Http\ApiV1\Modules\Notifications\Requests\PatchNotificationSettingRequest;
use App\Http\ApiV1\Modules\Notifications\Resources\NotificationEventsResource;
use App\Http\ApiV1\Modules\Notifications\Resources\NotificationSettingsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class NotificationSettingsController
{
    public function create(CreateNotificationSettingRequest $request, CreateNotificationSettingAction $action): Responsable
    {
        return NotificationSettingsResource::make($action->execute($request->validated()));
    }

    public function patch(int $id, PatchNotificationSettingRequest $request, PatchNotificationSettingAction $action): Responsable
    {
        return NotificationSettingsResource::make($action->execute($id, $request->validated()));
    }

    public function delete(int $id, DeleteNotificationSettingAction $action): Responsable
    {
        $action->execute($id);

        return new EmptyResource();
    }

    public function get(int $id, NotificationSettingsQuery $query): Responsable
    {
        return NotificationSettingsResource::make($query->findOrFail($id));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, NotificationSettingsQuery $query): Responsable
    {
        return NotificationSettingsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function getVariables(): Responsable
    {
        return NotificationEventsResource::collection(NotificationVariableData::all());
    }
}
