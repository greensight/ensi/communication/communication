<?php

namespace App\Http\ApiV1\Modules\Notifications\Controllers;

use App\Domain\Notifications\Actions\MassPatchNotificationsAction;
use App\Http\ApiV1\Modules\Notifications\Queries\NotificationsQuery;
use App\Http\ApiV1\Modules\Notifications\Requests\MassPatchNotificationsRequest;
use App\Http\ApiV1\Modules\Notifications\Resources\NotificationsResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Contracts\Support\Responsable;

class NotificationsController
{
    public function search(PageBuilderFactory $pageBuilderFactory, NotificationsQuery $query): Responsable
    {
        return NotificationsResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function massPatch(
        NotificationsQuery $query,
        MassPatchNotificationsRequest $request,
        MassPatchNotificationsAction $action
    ): Responsable {
        $action->execute($query->getEloquentBuilder(), $request->getFields());

        return new EmptyResource();
    }
}
