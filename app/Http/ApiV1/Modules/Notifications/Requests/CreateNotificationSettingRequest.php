<?php

namespace App\Http\ApiV1\Modules\Notifications\Requests;

use App\Domain\Notifications\Models\NotificationSetting;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationChannelEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateNotificationSettingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'string'],
            'event' => ['required', 'integer',
                Rule::enum(NotificationEventEnum::class),
                Rule::unique(NotificationSetting::class),
            ],
            'channels' => ['required', 'array', 'min:1'],
            'channels.*' => ['required_with:channels', 'integer', Rule::enum(NotificationChannelEnum::class)],
            'theme' => ['nullable', 'string'],
            'text' => ['required', 'string'],
        ];
    }
}
