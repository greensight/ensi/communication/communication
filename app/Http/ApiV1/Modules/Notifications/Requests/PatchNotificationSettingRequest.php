<?php

namespace App\Http\ApiV1\Modules\Notifications\Requests;

use App\Domain\Notifications\Models\NotificationSetting;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationChannelEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class PatchNotificationSettingRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['sometimes', 'string'],
            'event' => ['sometimes', 'integer',
                Rule::enum(NotificationEventEnum::class),
                Rule::unique(NotificationSetting::class)->ignore($this->getId()),
            ],
            'channels' => ['sometimes', 'array', 'min:1'],
            'channels.*' => ['required_with:channels', 'integer', Rule::enum(NotificationChannelEnum::class)],
            'theme' => ['nullable', 'string'],
            'text' => ['sometimes', 'string'],
        ];
    }
}
