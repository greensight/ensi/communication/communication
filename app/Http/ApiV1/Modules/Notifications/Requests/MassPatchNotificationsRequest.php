<?php

namespace App\Http\ApiV1\Modules\Notifications\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class MassPatchNotificationsRequest extends BaseFormRequest
{
    public function rules(): array
    {
        $baseRules = [
            'filter' => ['sometimes', 'array'],
            'fields' => ['sometimes', 'array'],
        ];

        return array_merge($baseRules, self::nestedRules('fields', PatchNotificationRequest::baseRules()));
    }

    public function getFields(): array
    {
        return $this->input('fields', []);
    }
}
