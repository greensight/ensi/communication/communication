<?php

namespace App\Http\ApiV1\Modules\Notifications\Requests;

use App\Http\ApiV1\Support\Requests\BaseFormRequest;

class PatchNotificationRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return self::baseRules();
    }

    public static function baseRules(): array
    {
        return [
            'is_viewed' => ['sometimes', 'boolean'],
        ];
    }
}
