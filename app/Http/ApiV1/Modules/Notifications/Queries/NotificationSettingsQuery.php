<?php

namespace App\Http\ApiV1\Modules\Notifications\Queries;

use App\Domain\Notifications\Models\NotificationSetting;
use Ensi\QueryBuilderHelpers\Filters\ArrayFilter;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class NotificationSettingsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(NotificationSetting::query());

        $this->allowedSorts(['id', 'event', 'name', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),

            AllowedFilter::exact('event'),
            ...ArrayFilter::make('channels')->contain(),

            ...StringFilter::make('name')->contain(),
            ...StringFilter::make('theme')->contain(),
            ...StringFilter::make('text')->contain(),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
