<?php

namespace App\Http\ApiV1\Modules\Notifications\Queries;

use App\Domain\Notifications\Models\Notification;
use Ensi\QueryBuilderHelpers\Filters\ArrayFilter;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class NotificationsQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Notification::query());

        $this->allowedSorts(['id', 'event', 'is_viewed', 'created_at', 'updated_at']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('customer_id'),

            AllowedFilter::exact('event'),
            ...ArrayFilter::make('channels')->contain(),

            ...StringFilter::make('theme')->contain(),
            ...StringFilter::make('text')->contain(),

            AllowedFilter::exact('is_viewed'),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
        ]);

        $this->defaultSort('id');
    }
}
