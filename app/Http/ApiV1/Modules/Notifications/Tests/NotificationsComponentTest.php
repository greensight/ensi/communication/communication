<?php

use App\Domain\Notifications\Models\Notification;
use App\Http\ApiV1\Modules\Notifications\Tests\Factories\MassPatchNotificationRequestFactory;
use App\Http\ApiV1\Modules\Notifications\Tests\Factories\NotificationRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationChannelEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/notifications:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $notifications = Notification::factory()
        ->forEachSequence(
            ['event' => NotificationEventEnum::ORDER_WAIT_PAY],
            ['event' => NotificationEventEnum::ORDER_NEW],
        )
        ->create();

    postJson('/api/v1/notifications:search', [
        "filter" => ["event" => NotificationEventEnum::ORDER_NEW],
        "sort" => ["-id"],
    ])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $notifications->last()->id)
        ->assertJsonPath('data.0.event', NotificationEventEnum::ORDER_NEW->value);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/notifications:search filter success", function (
    string $fieldKey,
    mixed $value,
    ?string $filterKey,
    mixed $filterValue,
) {
    /** @var Notification $notification */
    $notification = Notification::factory()->create($value ? [$fieldKey => $value] : []);

    postJson("/api/v1/notifications:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $notification->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $notification->id);
})->with([
    ['id', null, null, null],
    ['customer_id', 1, null, null],
    ['event', NotificationEventEnum::ORDER_WAIT_PAY->value, null, null],
    ['channels', [NotificationChannelEnum::EMAIL->value], 'channels_like', [NotificationChannelEnum::EMAIL->value, NotificationChannelEnum::SMS->value]],
    ['theme', 'Заказ успешно создан', 'theme_like', 'заказ'],
    ['text', 'Заказ успешно создан', 'text_like', 'заказ'],
    ['is_viewed', true, null, null],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/notifications:search sort success", function (string $sort) {
    Notification::factory()->create();
    postJson("/api/v1/notifications:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id', 'event', 'is_viewed', 'created_at', 'updated_at',
]);

test('POST /api/v1/notifications:mass-patch 200', function () {
    /** @var Notification $notification */
    $notification = Notification::factory()->create();
    /** @var Notification $otherNotification */
    $otherNotification = Notification::factory()->create();

    $fields = NotificationRequestFactory::new()->make();
    $request = MassPatchNotificationRequestFactory::new()
        ->withFilter(['id' => [$notification->id]])
        ->withFields($fields)
        ->make();

    postJson('/api/v1/notifications:mass-patch', $request)
        ->assertOk();

    assertDatabaseHas(Notification::class, array_merge(['id' => $notification->id], $fields));

    $otherNotificationData = prepareFieldsWithArray($otherNotification->toArray(), 'channels');

    assertDatabaseHas(Notification::class, $otherNotificationData);
});

test('POST /api/v1/notifications:mass-patch set viewed for customer 200', function () {
    $customerId = 1;
    /** @var Notification $notification */
    $notification = Notification::factory()->create(['is_viewed' => false, 'customer_id' => $customerId]);
    /** @var Notification $otherNotification */
    $otherNotification = Notification::factory()->create(['is_viewed' => false, 'customer_id' => $customerId]);

    $request = MassPatchNotificationRequestFactory::new()
        ->withFilter(['id' => $notification->id, 'customer_id' => $customerId])
        ->withFields(['is_viewed' => true])
        ->make();

    postJson('/api/v1/notifications:mass-patch', $request)
        ->assertOk();

    assertDatabaseHas(Notification::class, ['id' => $notification->id, 'is_viewed' => true]);
    assertDatabaseHas(Notification::class, ['id' => $otherNotification->id, 'is_viewed' => false]);
});
