<?php

namespace App\Http\ApiV1\Modules\Notifications\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class NotificationRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'is_viewed' => $this->faker->boolean(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
