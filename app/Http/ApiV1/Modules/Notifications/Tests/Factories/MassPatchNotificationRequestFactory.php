<?php

namespace App\Http\ApiV1\Modules\Notifications\Tests\Factories;

use Ensi\LaravelTestFactories\BaseApiFactory;

class MassPatchNotificationRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'filter' => [],
            'fields' => NotificationRequestFactory::new()->make(),
        ];
    }

    public function withFilter(array $filter): static
    {
        return $this->state(['filter' => $filter]);
    }

    public function withFields(array $fields): static
    {
        return $this->state(['fields' => $fields]);
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
