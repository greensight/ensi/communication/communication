<?php

namespace App\Http\ApiV1\Modules\Notifications\Tests\Factories;

use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationChannelEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class NotificationSettingRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->sentence(3),
            'event' => $this->faker->unique()->randomEnum(NotificationEventEnum::cases()),
            'channels' => $this->faker->randomList(fn () => $this->faker->randomEnum(NotificationChannelEnum::cases()), min: 1),
            'theme' => $this->faker->nullable()->sentence(3),
            'text' => $this->faker->text(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
