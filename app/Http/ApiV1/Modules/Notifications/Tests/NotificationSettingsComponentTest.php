<?php

use App\Domain\Notifications\Models\NotificationSetting;
use App\Http\ApiV1\Modules\Notifications\Tests\Factories\NotificationSettingRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'notifications');

test('POST /api/v1/notification-settings 201', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = NotificationSettingRequestFactory::new()->make();
    $channels = data_get($request, 'channels');

    $id = postJson('/api/v1/notification-settings', $request)
        ->assertCreated()
        ->assertJsonPath('data.channels', $channels)
        ->json('data.id');

    $assertData = array_merge(prepareFieldsWithArray($request, 'channels'), ['id' => $id]);

    assertDatabaseHas(NotificationSetting::class, $assertData);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/notification-settings 400', function () {
    $event = NotificationEventEnum::ORDER_WAIT_PAY;

    NotificationSetting::factory()->create(['event' => $event]);
    $request = NotificationSettingRequestFactory::new()->make(['event' => $event]);

    postJson('/api/v1/notification-settings', $request)
        ->assertBadRequest()
        ->assertJsonPath('errors.0.message', __('validation.unique', ['attribute' => 'event']));
});

test('DELETE /api/v1/notification-settings/{id} 200', function () {
    /** @var NotificationSetting $notificationSetting */
    $notificationSetting = NotificationSetting::factory()->create();

    deleteJson("/api/v1/notification-settings/{$notificationSetting->id}")
        ->assertOk();

    assertModelMissing($notificationSetting);
});

test('PATCH /api/v1/notification-settings/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var NotificationSetting $notificationSetting */
    $notificationSetting = NotificationSetting::factory()->create();
    $request = NotificationSettingRequestFactory::new()->make();

    patchJson("/api/v1/notification-settings/{$notificationSetting->id}", $request)
        ->assertJsonPath('data.id', $notificationSetting->id)
        ->assertOk();

    $assertData = array_merge(
        ['id' => $notificationSetting->id],
        prepareFieldsWithArray($request, 'channels'),
    );

    assertDatabaseHas(NotificationSetting::class, $assertData);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/notification-settings/{id} 400', function () {
    /** @var NotificationSetting $notificationSetting */
    $notificationSetting = NotificationSetting::factory()->create();
    /** @var NotificationSetting $currentNotificationSetting */
    $currentNotificationSetting = NotificationSetting::factory()->create();
    $request = NotificationSettingRequestFactory::new()->make(['event' => $notificationSetting->event]);

    patchJson("/api/v1/notification-settings/$currentNotificationSetting->id", $request)
        ->assertBadRequest()
        ->assertJsonPath('errors.0.message', __('validation.unique', ['attribute' => 'event']));
});

test('PATCH /api/v1/notification-settings/{id} 404', function () {
    $request = NotificationSettingRequestFactory::new()->make();
    patchJson('/api/v1/notification-settings/404', $request)
        ->assertNotFound();
});

test('GET /api/v1/notification-settings/{id} 200', function () {
    /** @var NotificationSetting $notificationSetting */
    $notificationSetting = NotificationSetting::factory()->create();

    getJson("/api/v1/notification-settings/{$notificationSetting->id}")
        ->assertJsonPath('data.id', $notificationSetting->id)
        ->assertOk();
});

test('GET /api/v1/notification-settings/{id} 404', function () {
    getJson('/api/v1/notification-settings/404')
        ->assertNotFound();
});

test('GET /api/v1/notification-setting-variables 200', function () {
    getJson('/api/v1/notification-setting-variables')
        ->assertOk()
        ->assertJsonStructure(['data' => [['event', 'variables' => [['id', 'title']]]]]);
});
