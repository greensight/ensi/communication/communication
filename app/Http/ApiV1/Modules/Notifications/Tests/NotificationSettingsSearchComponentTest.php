<?php

use App\Domain\Notifications\Models\NotificationSetting;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationChannelEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\NotificationEventEnum;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component', 'notifications');

test('POST /api/v1/notification-settings:search 200', function () {
    $notificationSettings = NotificationSetting::factory()
        ->forEachSequence(
            ['event' => NotificationEventEnum::ORDER_WAIT_PAY],
            ['event' => NotificationEventEnum::ORDER_NEW],
        )
        ->create();

    postJson('/api/v1/notification-settings:search', [
        "filter" => ["event" => NotificationEventEnum::ORDER_NEW],
        "sort" => ["-id"],
    ])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $notificationSettings->last()->id)
        ->assertJsonPath('data.0.event', NotificationEventEnum::ORDER_NEW->value);
});

test("POST /api/v1/notification-settings:search filter success", function (
    string $fieldKey,
    mixed $value,
    ?string $filterKey,
    mixed $filterValue,
) {
    /** @var NotificationSetting $notificationSetting */
    $notificationSetting = NotificationSetting::factory()->create($value ? [$fieldKey => $value] : []);

    postJson("/api/v1/notification-settings:search", ["filter" => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $notificationSetting->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertStatus(200)
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $notificationSetting->id);
})->with([
    ['id', null, null, null],
    ['event', NotificationEventEnum::ORDER_WAIT_PAY->value, null, null],
    ['channels', [NotificationChannelEnum::EMAIL->value], 'channels_like', [NotificationChannelEnum::EMAIL->value, NotificationChannelEnum::SMS->value]],
    ['name', 'Уведомление при создании заказа', 'name_like', 'заказ'],
    ['theme', 'Заказ успешно создан', 'theme_like', 'заказ'],
    ['text', 'Заказ успешно создан', 'text_like', 'заказ'],
    ['created_at', '2022-04-20', 'created_at_gte', '2022-04-19'],
    ['created_at', '2022-04-20', 'created_at_lte', '2022-04-21'],
    ['updated_at', '2022-04-20', 'updated_at_gte', '2022-04-19'],
    ['updated_at', '2022-04-20', 'updated_at_lte', '2022-04-21'],
]);

test("POST /api/v1/notification-settings:search sort success", function (string $sort) {
    NotificationSetting::factory()->create();
    postJson("/api/v1/notification-settings:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id', 'event', 'name', 'created_at', 'updated_at',
]);
