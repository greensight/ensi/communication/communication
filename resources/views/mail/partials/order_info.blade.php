<table class="email-order-total" style="border-collapse: collapse; border-spacing: 0; box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top; width: 100%;">
    <tr style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
        <th class="container__cell" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 0; padding-left: 24px; padding-right: 24px; padding-top: 0; text-align: center;">
            <table style="border-collapse: collapse; border-spacing: 0; box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top; width: 100%;">
                <tr class="email-order-total__row" style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                    <th colspan="2" class="email-order-total__cell" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 0; padding-right: 0; padding-top: 8px; text-align: left; width: 50%;">
                        <h3 class="email-order-total__title" style="Margin: 0; Margin-bottom: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 18px; font-weight: bold; line-height: 1.4; margin: 0; margin-bottom: 0; max-width: 100%; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; word-wrap: normal;">Информация о заказе №{{ $order_number }} от {{ $create_date }}</h3>
                    </th>
                </tr>
                <tr class="email-order-total__row" style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                    <th class="email-order-total__cell email-order-total__cell--parameter" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 0; padding-right: 0; padding-top: 8px; text-align: left; width: 50%;">
                        Получатель
                    </th>
                    <th class="email-order-total__cell email-order-total__cell--value" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 16px; padding-right: 0; padding-top: 8px; text-align: left; width: 50%;">
                        {{ $receiver_name }}<br>
                        {{ $receiver_phone }}<br>
                        {{ $receiver_email }}
                    </th>
                </tr>
                <tr class="email-order-total__row" style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                    <th class="email-order-total__cell email-order-total__cell--parameter" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 0; padding-right: 0; padding-top: 8px; text-align: left; width: 50%;">
                        Адрес доставки
                    </th>
                    <th class="email-order-total__cell email-order-total__cell--value" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 16px; padding-right: 0; padding-top: 8px; text-align: left; width: 50%;">
                        {{ $delivery_address }}
                    </th>
                </tr>
                @if ($delivery_comment)
                <tr class="email-order-total__row" style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                    <th class="email-order-total__cell email-order-total__cell--parameter" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 0; padding-right: 0; padding-top: 8px; text-align: left; width: 50%;">
                        Комментарий к заказу
                    </th>
                    <th class="email-order-total__cell email-order-total__cell--value" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 16px; padding-right: 0; padding-top: 8px; text-align: left; width: 50%;">
                        {{ $delivery_comment }}
                    </th>
                </tr>
                @endif
                <tr class="email-order-total__row" style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                    <th class="email-order-total__cell email-order-total__cell--parameter" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 0; padding-right: 0; padding-top: 8px; text-align: left; width: 50%;">
                        Оплата
                    </th>
                    <th class="email-order-total__cell email-order-total__cell--value" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 16px; padding-right: 0; padding-top: 8px; text-align: left; width: 50%;">
                        {{ $order_cost }} руб., оплата онлайн
                    </th>
                </tr>
            </table>
        </th>
    </tr>
</table>
