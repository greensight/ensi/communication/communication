<table class="table email-order-table" style="border-collapse: collapse; border-spacing: 0; box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top; width: 100%;">
    <tr style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
        <th class="container__cell email-order-table__container-cell" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 0; padding-left: 24px; padding-right: 24px; padding-top: 0; text-align: center;">
            <table style="border-collapse: collapse; border-spacing: 0; box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top; width: 100%;">
                <tr style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                    <th colspan="5" class="email-order-table__head-title" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 16px; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center;">
                        <h3 class="text-l" style="Margin: 0; Margin-bottom: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 18px; font-weight: bold; line-height: 1.4; margin: 0; margin-bottom: 0; max-width: 100%; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; word-wrap: normal;">Состав заказа</h3>
                    </th>
                </tr>
                <tr class="email-order-table__head-row" style="background: #F0F0F0; box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: middle;">
                    <th colspan="2" class="table__head email-order-table__main" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 16px; padding-right: 8px; padding-top: 8px; text-align: left;">Наименование</th>
                    <th class="table__head email-order-table__price" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 8px; padding-right: 8px; padding-top: 8px; text-align: center;">Стоимость</th>
                    <th class="table__head email-order-table__amount" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 8px; padding-right: 8px; padding-top: 8px; text-align: center;">Кол&#8209;во</th>
                    <th class="table__head email-order-table__total" style="Margin: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 8px; padding-left: 8px; padding-right: 0; padding-top: 8px; text-align: center;">Сумма</th>
                </tr>
                @foreach ($basket_items as $item)
                    <tr class="email-order-table__row" style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: middle;">
                        @if ($item['image_url'])
                            <th class="table__cell email-order-table__cell email-order-table__photo" style="Margin: 0; border-bottom: 1px solid #F0F0F0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 16px; padding-left: 0; padding-right: 0; padding-top: 16px; text-align: center; width: 64px;">
                                <img height="64"
                                     src="{{ $item['image_url'] }}"
                                     alt="{{ $item['name'] }}"
                                     style="-ms-interpolation-mode: bicubic; box-sizing: border-box; clear: both; display: inline-block; max-height: 64px; max-width: 100%; outline: none; text-decoration: none; width: auto;">
                            </th>
                        @endif
                        <th class="table__cell email-order-table__cell email-order-table__main" style="Margin: 0; border-bottom: 1px solid #F0F0F0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 16px; padding-left: 8px; padding-right: 8px; padding-top: 16px; text-align: left;">
                            <p class="email-order-table__name" style="Margin: 0; Margin-bottom: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; margin-bottom: 0; max-width: 100%; padding-bottom: 8px; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left;">
                                {{ $item['name'] }}
                            </p>
                        </th>
                        <th class="table__cell email-order-table__cell email-order-table__price" style="Margin: 0; border-bottom: 1px solid #F0F0F0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 16px; padding-left: 8px; padding-right: 8px; padding-top: 16px; text-align: center;">
                            <p class="email-order-table__mobile-caption" style="Margin: 0; Margin-bottom: 0; box-sizing: border-box; color: #333; display: none; font-family: Arial, 'Roboto', sans-serif; font-size: 0; font-weight: normal; line-height: 0; margin: 0; margin-bottom: 0; max-height: 0; max-width: 100%; mso-hide: all; overflow: hidden; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center; width: 0;">Стоимость</p>
                            {{ $item['price'] }}&nbsp;руб.
                        </th>
                        <th class="table__cell email-order-table__cell email-order-table__amount" style="Margin: 0; border-bottom: 1px solid #F0F0F0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 16px; padding-left: 8px; padding-right: 8px; padding-top: 16px; text-align: center;">
                            <p class="email-order-table__mobile-caption" style="Margin: 0; Margin-bottom: 0; box-sizing: border-box; color: #333; display: none; font-family: Arial, 'Roboto', sans-serif; font-size: 0; font-weight: normal; line-height: 0; margin: 0; margin-bottom: 0; max-height: 0; max-width: 100%; mso-hide: all; overflow: hidden; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center; width: 0;">Количество</p>
                            {{ $item['qty'] }}
                        </th>
                        <th class="table__cell email-order-table__cell email-order-table__total" style="Margin: 0; border-bottom: 1px solid #F0F0F0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: bold; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 16px; padding-left: 8px; padding-right: 0; padding-top: 16px; text-align: center;">
                            <p class="email-order-table__mobile-caption" style="Margin: 0; Margin-bottom: 0; box-sizing: border-box; color: #333; display: none; font-family: Arial, 'Roboto', sans-serif; font-size: 0; font-weight: normal; line-height: 0; margin: 0; margin-bottom: 0; max-height: 0; max-width: 100%; mso-hide: all; overflow: hidden; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center; width: 0;">Сумма</p>
                            {{ $item['sum'] }}&nbsp;руб.
                        </th>
                    </tr>
                @endforeach
            </table>
        </th>
    </tr>
</table>
