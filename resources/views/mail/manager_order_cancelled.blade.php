@extends('layouts.email')

@section('preheader')
    Заказ №{{ $order_number }} отменен
@endsection

@section('content')
    <table class="container email-order bg-body__container" align="center" style="Margin: 0 auto; background-color: #ffffff; border-collapse: collapse; border-spacing: 0; box-sizing: border-box; margin: 0 auto; max-width: 600px; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: inherit; vertical-align: top; width: 600px;">
        <tr style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
            <th style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center;">
                @include('mail.partials.spacer', ['size' => 16])

                <img class="icon" src="{{ absolute_url('/email/email_icon_cancel.png') }}" alt="Заказ отменен" style="-ms-interpolation-mode: bicubic; box-sizing: border-box; clear: both; display: inline-block; max-width: 48px; outline: none; text-decoration: none; width: auto;">

                @include('mail.partials.spacer', ['size' => 10])

                <h1 class="email-order__title" style="Margin: 0; Margin-bottom: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 20px; font-weight: bold; line-height: 1.4; margin: 0; margin-bottom: 0; max-width: 100%; padding-bottom: 0; padding-left: 24px; padding-right: 24px; padding-top: 0; text-align: center; word-wrap: normal;">Заказ отменен</h1>

                @include('mail.partials.spacer', ['size' => 16])

                @include('mail.partials.divider')

                @include('mail.partials.spacer', ['size' => 8])

                @include('mail.partials.order_info', compact(
                    'order_number',
                    'create_date',
                    'receiver_name',
                    'receiver_phone',
                    'receiver_email',
                    'delivery_address',
                    'delivery_comment',
                    'order_cost'
                ))

                @include('mail.partials.spacer', ['size' => 8])

                @include('mail.partials.divider')

                @include('mail.partials.spacer', ['size' => 16])

                @include('mail.partials.basket_info', compact('basket_items'))

                @include('mail.partials.spacer', ['size' => 16])

                @include('mail.partials.order_total', compact(
                    'total_count',
                    'items_cost',
                    'delivery_cost',
                    'order_cost'
                ))

                @include('mail.partials.spacer', ['size' => 8])
            </th>
        </tr>
    </table>
@endsection
