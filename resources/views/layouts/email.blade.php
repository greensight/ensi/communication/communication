<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
        "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml"
      lang="{{ str_replace('_', '-', app()->getLocale()) }}"
      xml:lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="format-detection" content="telephone=no">

    <title>Изменение статуса заказа</title>

    <style type="text/css">
        @media (max-width: 767px) {
            *.text-l-mobile {
                text-align: left !important;
            }
        }

        @media (max-width: 767px) {
            *.text-c-mobile {
                text-align: center !important;
            }
        }

        @media (max-width: 767px) {
            *.text-r-mobile {
                text-align: right !important;
            }
        }

        @media (max-width: 767px) {
            .hidden-desktop {
                display: block !important;
                width: auto !important;
                overflow: visible !important;
                max-height: none !important;
                font-size: inherit !important;
                line-height: inherit !important;
            }
        }

        @media (max-width: 767px) {
            table.body .hidden-mobile {
                display: none !important;
                width: 0;
                mso-hide: all;
                overflow: hidden;
            }
        }

        @media (max-width: 767px) {
            .spacer__cell-mobile {
                display: block !important;
                width: auto !important;
                overflow: visible !important;
                max-height: none !important;
            }
        }

        @media (max-width: 767px) {
            h1 {
                font-size: 18px !important;
            }
        }

        @media (max-width: 767px) {
            h2 {
                font-size: 16px !important;
            }
        }

        @media (max-width: 767px) {
            h3 {
                font-size: 16px !important;
            }
        }

        @media (max-width: 767px) {
            body,
            table.body,
            p,
            td,
            th {
                font-size: 13px !important;
            }
        }

        @media all {
            a:hover {
                color: #00985F !important;
            }
        }

        @media only screen {
            a[x-apple-data-detectors] {
                color: inherit !important;
                text-decoration: none !important;
                font-size: inherit !important;
                font-family: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
            }
        }

        @media all {
            .btn__link:hover {
                color: #ffffff !important;
                background: #73dfdb !important;
            }
        }

        @media (max-width: 479px) {
            .btn__link {
                min-width: 280px !important;
                width: 100% !important;
            }
        }

        @media (max-width: 767px) {
            .container {
                width: 100% !important;
            }
        }

        @media (max-width: 767px) {
            .container__cell {
                padding-left: 3.5% !important;
                padding-right: 3.5% !important;
            }
        }

        @media (max-width: 767px) {
            .container__cell--no-padding {
                padding-left: 0 !important;
                padding-right: 0 !important;
            }
        }

        @media (max-width: 767px) {
            .container-inner__cell {
                padding-left: 3.5% !important;
                padding-right: 3.5% !important;
            }
        }

        @media all {
            .ui-kit__title::after {
                position: absolute;
                right: 0;
                counter-increment: uiSectionCounter;
                content: '\2014  ' counter(uiSectionCounter);
                display: inline-block;
            }
        }

        @media all {
            .ui-kit__secondary--num::before {
                counter-increment: uiSmallDescCounter;
                content: counter(uiSmallDescCounter) '. ';
            }
        }

        @media (max-width: 767px) {
            .hero-image__cell {
                height: 140px !important;
            }
        }

        @media (max-width: 767px) {
            .bg__inner {
                padding-left: 3.5% !important;
                padding-right: 3.5% !important;
            }
        }

        @media (max-width: 479px) {
            .header__container {
                display: block !important;
            }
        }

        @media (max-width: 479px) {
            .header__container tbody {
                display: block !important;
                width: 100% !important;
            }
        }

        @media (max-width: 479px) {
            .header__row {
                display: block !important;
            }
        }

        @media (max-width: 479px) {
            .header__cell--info {
                padding-top: 0 !important;
            }
        }

        @media (max-width: 479px) {
            .header__cell {
                display: block !important;
                width: auto !important;
                text-align: right !important;
            }
        }

        @media (max-width: 479px) {
            .header__text {
                text-align: right !important;
            }
        }

        @media (max-width: 479px) {
            .total-table {
                float: none !important;
                width: 100% !important;
            }
        }

        @media (max-width: 767px) {
            .socials {
                margin: auto !important;
            }
        }

        @media (max-width: 767px) {
            .socials__cell {
                text-align: center !important;
            }
        }

        @media (max-width: 767px) {
            .email-order__title,
            .email-order__text {
                padding-left: 3.5% !important;
                padding-right: 3.5% !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__container-cell {
                display: block !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__head-row {
                display: none !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__row {
                display: block !important;
                padding: 8px 0 !important;
                border-bottom: 1px solid #F0F0F0 !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__cell {
                display: block !important;
                width: auto !important;
                padding: 4px 0 !important;
                border: none !important;
                text-align: center !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__photo {
                padding-bottom: 16px !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__price {
                line-height: 1.5 !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__amount {
                line-height: 1.5 !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__total {
                text-align: center !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__name {
                text-align: center !important;
                font-weight: bold !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__code {
                text-align: center !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__type {
                text-align: center !important;
            }
        }

        @media (max-width: 479px) {
            .email-order-table__mobile-caption {
                display: block !important;
                padding-bottom: 8px;
                width: auto !important;
                overflow: visible !important;
                max-height: none !important;
                font-size: 12px !important;
                line-height: inherit !important;
                color: #979797 !important;
            }
        }

        @media (max-width: 639px) {
            .email-order-total__row {
                display: block !important;
            }
        }

        @media (max-width: 639px) {
            .email-order-total__cell {
                display: block !important;
                width: auto !important;
                border: none !important;
                padding: 4px 0 !important;
            }
        }

        @media (max-width: 639px) {
            .email-order-total__cell--value {
                padding-left: 0 !important;
            }
        }

        @media (max-width: 639px) {
            .email-order-summary {
                width: 100% !important;
            }
        }

        @media (max-width: 639px) {
            .email-order-summary__cell--parameter {
                text-align: left !important;
            }
        }
    </style>
</head>

<body class="bg-body" style="-ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%; Margin: 0; background-color: #ffffff; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; min-width: 100%; padding: 0; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center; width: 100% !important;">
<span class="preheader" style="box-sizing: border-box; color: transparent; display: none !important; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; mso-hide: all !important; opacity: 0; overflow: hidden; visibility: hidden;">
    @yield('preheader')
</span>

<table class="body" style="Margin: 0; border-collapse: collapse; border-spacing: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; height: 100%; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center; vertical-align: top; width: 100%;">
    <tr style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
        <td class="center" align="center" style="Margin: 0; border-collapse: collapse !important; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center; vertical-align: top; word-wrap: break-word;">
            <center style="box-sizing: border-box; width: 100%;">

                <table class="header" style="border-collapse: collapse; border-spacing: 0; box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top; width: 100%;">
                    <tr style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                        <th style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center;">
                            <table class="container header__container" align="center" style="Margin: 0 auto; border-bottom: 1px solid #F0F0F0; border-collapse: collapse; border-spacing: 0; box-sizing: border-box; margin: 0 auto; max-width: 600px; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: inherit; vertical-align: top; width: 600px;">
                                <tr class="header__row" style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                                    <th class="container__cell header__cell" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 16px; padding-left: 24px; padding-right: 24px; padding-top: 16px; text-align: left; width: 50%;">
                                        <a class="img-link header__logo" href="{{ absolute_url('/') }}" title="Перейти на сайт Ensi" style="box-sizing: border-box; color: #333; display: inline-block; font-family: Arial, 'Roboto', sans-serif; font-weight: normal; line-height: 1.4; padding: 0; text-align: left; text-decoration: none;">
                                            <img class="header__logo-img" src="{{ absolute_url('/email/email_logo.png') }}" alt="Ensi" style="-ms-interpolation-mode: bicubic; border: none; box-sizing: border-box; clear: both; display: inline-block; max-width: 130px; outline: none; text-decoration: none; width: auto;">
                                        </a>
                                    </th>
                                    <th class="container__cell header__cell header__cell--info" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 16px; padding-left: 16px; padding-right: 24px; padding-top: 16px; text-align: left; width: 50%;">
                                        <a class="header__phone" href="tel:88007005800" style="box-sizing: border-box; color: #333; font-family: Arial-Black, 'Roboto', sans-serif; font-size: 20px; font-weight: 900; line-height: 1.2; padding: 0; padding-bottom: 8px; text-align: left; text-decoration: none;">8 800 700-58-00</a>
                                        <p class="header__text" style="Margin: 0; Margin-bottom: 0; box-sizing: border-box; color: #7C808C; font-family: Arial, 'Roboto', sans-serif; font-size: 14px; font-weight: normal; line-height: 1.4; margin: 0; margin-bottom: 0; max-width: 100%; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left;">Круглосуточная поддержка клиентов</p>
                                    </th>
                                </tr>
                            </table>
                        </th>
                    </tr>
                </table>

                @yield('content')

                <table class="footer" style="border-collapse: collapse; border-spacing: 0; box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top; width: 100%;">
                    <tr style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                        <th style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center;">
                            <table class="container footer__container" align="center" style="Margin: 0 auto; border-collapse: collapse; border-spacing: 0; border-top: 1px solid #F0F0F0; box-sizing: border-box; margin: 0 auto; max-width: 600px; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: inherit; vertical-align: top; width: 600px;">
                                <tr style="box-sizing: border-box; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: left; vertical-align: top;">
                                    <th class="container__cell footer__text" style="Margin: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 16px; font-weight: normal; line-height: 1.4; margin: 0; max-width: 100%; padding-bottom: 32px; padding-left: 24px; padding-right: 24px; padding-top: 32px; text-align: center;">
                                        <h3 style="Margin: 0; Margin-bottom: 0; box-sizing: border-box; color: #333; font-family: Arial, 'Roboto', sans-serif; font-size: 18px; font-weight: bold; line-height: 1.4; margin: 0; margin-bottom: 0; max-width: 100%; padding-bottom: 0; padding-left: 0; padding-right: 0; padding-top: 0; text-align: center; word-wrap: normal;">Спасибо что выбрали нас!</h3>
                                    </th>
                                </tr>
                            </table>
                        </th>
                    </tr>
                </table>


            </center>
        </td>
    </tr>
</table>

<div style="box-sizing: border-box; display: none; font: 15px courier; line-height: 0; white-space: nowrap;"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
</div>
</body>
</html>
