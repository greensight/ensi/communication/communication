<?php

use App\Domain\Events\Actions\Customers\CustomerAuth\CustomerAuthGeneratePasswordTokenAction;
use App\Domain\Events\Actions\Customers\CustomerAuth\SendMail\UserUpdatedAction;
use App\Domain\Events\Actions\Customers\Customers\ChangeEmailAction;
use App\Domain\Events\Actions\Customers\Customers\SendMail\CustomerUpdatedAction;
use App\Domain\Events\Actions\Customers\ResetPasswordAction;
use App\Domain\Events\Actions\Customers\ResetPasswordSuccessAction;
use App\Domain\Events\Actions\Units\AdminAuth\SendEmail\DeactivatedAction;
use App\Domain\Events\Actions\Units\AdminAuth\SendEmail\SettingPasswordAction;
use App\Domain\Kafka\Actions\Listen\Customers\ListenSendConfirmationCodeAction;
use App\Domain\Kafka\Actions\Listen\Orders\ListenDeliveryAction;
use App\Domain\Kafka\Actions\Listen\Orders\ListenOrderAction;
use App\Domain\Kafka\Actions\Listen\Units\ListenAdminUserAction;
use Ensi\LaravelInitialEventPropagation\RdKafkaConsumerMiddleware;
use Ensi\LaravelMetrics\Kafka\KafkaMetricsMiddleware;

return [
    /*
    | Optional, defaults to empty array.
    | Array of global middleware fully qualified class names.
    */
    'global_middleware' => [ RdKafkaConsumerMiddleware::class, KafkaMetricsMiddleware::class ],
    'stop_signals' => [SIGTERM, SIGINT],

    'processors' => [
        [
            'topic' => 'admin-generated-password-token',
            'consumer' => 'default',
            'type' => 'action',
            'class' => SettingPasswordAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'customer-password-reset',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ResetPasswordAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'customer-password-reset-success',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ResetPasswordSuccessAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'customer-generated-password-token',
            'consumer' => 'default',
            'type' => 'action',
            'class' => CustomerAuthGeneratePasswordTokenAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'admin-user-deactivated',
            'consumer' => 'default',
            'type' => 'action',
            'class' => DeactivatedAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'admin-users',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenAdminUserAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'customer-changes-email',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ChangeEmailAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'customer-user-updated',
            'consumer' => 'default',
            'type' => 'action',
            'class' => UserUpdatedAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'customer-updated',
            'consumer' => 'default',
            'type' => 'action',
            'class' => CustomerUpdatedAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'orders',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenOrderAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'deliveries',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenDeliveryAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
        [
            'topic' => 'send-confirmation-code',
            'consumer' => 'default',
            'type' => 'action',
            'class' => ListenSendConfirmationCodeAction::class,
            'queue' => false,
            'consume_timeout' => 5000,
        ],
    ],
];
