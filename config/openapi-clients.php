<?php

return [
    'communication' => [
        'internal-messenger' => [
            'base_uri' => env('COMMUNICATION_INTERNAL_MESSAGES_SERVICE_HOST') . '/api/v1',
        ],
    ],
    'orders' => [
        'oms' => [
            'base_uri' => env('ORDERS_OMS_SERVICE_HOST') . '/api/v1',
        ],
    ],
    'catalog' => [
        'pim' => [
            'base_uri' => env('CATALOG_PIM_SERVICE_HOST') . '/api/v1',
        ],
        'offers' => [
            'base_uri' => env('CATALOG_OFFERS_SERVICE_HOST') . '/api/v1',
        ],
    ],
    'units' => [
        'bu' => [
            'base_uri' => env('UNITS_BU_SERVICE_HOST') . '/api/v1',
        ],
        'admin-auth' => [
            'base_uri' => env('UNITS_ADMIN_AUTH_SERVICE_HOST') . '/api/v1',
        ],
    ],
    'customers' => [
        'customer-auth' => [
            'base_uri' => env('CUSTOMERS_CUSTOMER_AUTH_SERVICE_HOST') . '/api/v1',
        ],
        'customers' => [
            'base_uri' => env('CUSTOMERS_CUSTOMERS_SERVICE_HOST') . '/api/v1',
        ],
    ],
    'logistic' => [
        'logistic' => [
            'base_uri' => env('LOGISTIC_LOGISTIC_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
