<?php

// Публичный хост фронта админки
$adminGuiHost = trim(env('ADMIN_GUI_FRONTEND_HOST', ''), '/');
// Публичный хост фронта витрины. Сейчас тут стоит заглушка.
$frontHost = trim(env('FRONTEND_HOST', ''), '/');

return [
    'admin_gui_urls' => [
        'set_password' => $adminGuiHost . '/set-password?token=',
    ],
    'front_urls' => [
        'set_password' => $frontHost . '/set-password?token=',
        'reset_password' => $frontHost . '/reset-password?token=',
    ],
];
