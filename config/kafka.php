<?php

// configurattion options can be found here: https://github.com/edenhill/librdkafka/blob/master/CONFIGURATION.md
// if an option is set to null it is ignored.
$contour = env('KAFKA_CONTOUR', 'local');

return [
    'connections' => [
        'default' => [
            'settings' => [
                'metadata.broker.list' => env('KAFKA_BROKER_LIST'),
                'security.protocol' => env('KAFKA_SECURITY_PROTOCOL', 'plaintext'),
                'sasl.mechanisms' => env('KAFKA_SASL_MECHANISMS'),
                'sasl.username' => env('KAFKA_SASL_USERNAME'),
                'sasl.password' => env('KAFKA_SASL_PASSWORD'),
                'log_level' => env('KAFKA_DEBUG', false) ? (string)LOG_DEBUG : (string)LOG_INFO,
                'debug' => env('KAFKA_DEBUG', false) ? 'all' : null,
            ],
            'topics' => [
                'admin-generated-password-token' => $contour . '.admin-auth.fact.generated-password-token.1',
                'admin-user-deactivated' => $contour . '.admin-auth.fact.deactivated-user.1',
                'admin-users' => $contour . '.admin-auth.fact.users.1',

                'customer-password-reset' => $contour . '.customer-auth.fact.password-reset.1',
                'customer-password-reset-success' => $contour . '.customer-auth.fact.password-reset-success.1',
                'customer-generated-password-token' => $contour . '.customer-auth.fact.generated-password-token.1',
                'customer-user-updated' => $contour . '.customer-auth.fact.user-updated.1',
                'send-confirmation-code' => $contour . '.customer-auth.command.send-confirmation-code.1',

                'customer-changes-email' => $contour . '.customers.fact.changes-email.1',
                'customer-updated' => $contour . '.customers.fact.customer-updated.1',

                'orders' => $contour . '.orders.fact.orders.1',
                'deliveries' => $contour . '.orders.fact.deliveries.1',

                'create-message' => $contour . '.communication.fact.create-message.1',
            ],
        ],
    ],
    'consumers' => [
        'default' => [
            'connection' => 'default',
            'additional-settings' => [
                'group.id' => env('KAFKA_CONSUMER_GROUP_ID', env('APP_NAME')),
                'enable.auto.commit' => true,
                'auto.offset.reset' => 'beginning',
            ],
        ],
    ],
    'producers' => [
        'default' => [
            'connection' => 'default',
            'additional-settings' => [
                'compression.codec' => env('KAFKA_PRODUCER_COMPRESSION_CODEC', 'snappy'),
            ],
        ],
    ],
];
