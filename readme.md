# Ensi Communication Manager

Название: Ensi Communication Manager  
Домен: Communication  
Назначение: Управление коммуникациями  

## Разработка сервиса

Инструкцию описывающую разворот, запуск и тестирование сервиса на локальной машине можно найти в отдельном документе в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

Регламент работы над задачами тоже находится в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/guid/regulations)

## Структура сервиса

Почитать про структуру сервиса можно [здесь](https://docs.ensi.tech/backend-guides/principles/service-structure)

## Зависимости

| Название | Описание                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | Переменные окружения |
| --- |---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------| --- |
| PostgreSQL | Основная БД сервиса                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |
| SMTP-сервер | Сервер для рассылки электронных писем                                                                                                                                                                                                                                                                                                                                                                                                                                                             | MAIL_MAILER<br/>MAIL_HOST<br/>MAIL_PORT<br/>MAIL_USERNAME<br/>MAIL_PASSWORD |
| Kafka | Брокер сообщений. <br/>Producer осуществляет запись в следующие топики:<br/> - `<контур>.communication.command.create-message.1`<br/> Consumer слушает следующие топики:<br/> - `<контур>.admin-auth.fact.generated-password-token.1` <br/> - `<контур>.customer-auth.fact.generated-password-token.1` <br/> - `<контур>.admin-auth.fact.deactivated-user.1` <br/> - `<контур>.admin-auth.fact.users.1` <br/> - `<контур>.orders.fact.orders.1` <br/> - `<контур>.customers.fact.changes-email.1` | KAFKA_CONTOUR<br/>KAFKA_BROKER_LIST<br/>KAFKA_SECURITY_PROTOCOL<br/>KAFKA_SASL_MECHANISMS<br/>KAFKA_SASL_USERNAME<br/>KAFKA_SASL_PASSWORD<br/> |
| **Сервисы Ensi** | **Сервисы Ensi, с которыми данный сервис коммуницирует**                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| Units | Ensi Admin Auth<br/>Ensi Business Units                                                                                                                                                                                                                                                                                                                                                                                                                                                           | ADMIN_AUTH_SERVICE_HOST<br/>UNITS_BU_SERVICE_HOST |
| Orders | Ensi OMS                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | ORDERS_OMS_SERVICE_HOST |
| Catalog | Ensi PIM                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          | CATALOG_PIM_SERVICE_HOST |
| Customers | Ensi Customers                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | CUSTOMERS_CUSTOMERS_SERVICE_HOST |
| Logistic | Ensi Logistic | LOGISTIC_LOGISTIC_SERVICE_HOST |

## Среды

### Test

CI: https://jenkins-infra.ensi.tech/job/ensi-stage-1/job/communication/job/communication/  
URL: https://communication-master-dev.ensi.tech/docs/swagger  

### Preprod

Отсутствует

### Prod

Отсутствует

## Контакты

Команда поддерживающая данный сервис: https://gitlab.com/groups/greensight/ensi/-/group_members  
Email для связи: mail@greensight.ru

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
