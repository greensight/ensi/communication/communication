<?php

namespace Tests;

use Ensi\CustomersClient\Api\CustomersApi;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\OffersClient\Api\OffersApi;
use Ensi\OmsClient\Api\DeliveriesApi;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\PimClient\Api\ProductsApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== Customers ===============

    // region service Customers
    protected function mockCustomersCustomersApi(): MockInterface|CustomersApi
    {
        return $this->mock(CustomersApi::class);
    }
    // endregion

    // =============== Catalog ===============

    // region service Pim
    protected function mockPimProductsApi(): MockInterface|ProductsApi
    {
        return $this->mock(ProductsApi::class);
    }
    // endregion

    // region service Offers
    protected function mockOffersOffersApi(): MockInterface|OffersApi
    {
        return $this->mock(OffersApi::class);
    }
    // endregion

    // =============== Orders ===============

    // region service OMS
    public function mockOmsOrdersApi(): MockInterface|OrdersApi
    {
        return $this->mock(OrdersApi::class);
    }

    public function mockOmsDeliveriesApi(): MockInterface|DeliveriesApi
    {
        return $this->mock(DeliveriesApi::class);
    }
    // endregion

    // =============== Logistic ===============

    // region service Logistic
    public function mockLogisticGeosApi(): MockInterface|GeosApi
    {
        return $this->mock(GeosApi::class);
    }
    // endregion
}
